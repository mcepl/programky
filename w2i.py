#!/usr/bin/python
# -*- coding: utf-8 -*-
# $Log$
# Revision 1.8  2007/01/08 23:50:31  matej
# Different changes.
#
# Revision 1.7  2000/10/20 07:25:14  matej
# Included option for cleanin-up (de-diacriticization) to ASCII.
#
# Revision 1.6  2000/06/08 05:49:26  matej
# V nejelp��žš��ž��ž��žÂ­ch UNIXov��žË�ťch tradic��žÂ­ch skript ne��žšÂekne nic, kdy��žš�šž nen��žÂ­ ��žš�šž��ž��ž��ždn��žË�ť probl��ž�š m.
#
# Revision 1.5  2000/04/04 18:45:12  matej
# os.rename fails on different devices (when working with file on Win
# disk and temporary file is in Linux /tmp directory) - used
# shutils.copyfile instead.
#
# Revision 1.4  2000/03/12 12:52:54  matej
# P��žšÂedĂ��žÂl��ž��ž��žno z if...then...else struktury na data-drive (ESR m��ž��ž��ž
# pravdu!). P��žšÂi tom p��žšÂid��ž��ž��žna podpora pro PC Latin 2 a o��žš��ž��žet��žšÂen��žÂ­ chyby bĂ��žÂhu
# CSTOCS.
#
# Revision 1.3  2000/03/10 19:05:34  matej
# DvĂ��žÂ drobn��ž�š  chybiĂ��žÂky - v��žË�ťstup do stderr nekonĂ��žÂil \n a ��žš��ž��žpatnĂ��žÂ zarovann��žË�ť
# v��žË�ťpis helpu.
#
# Revision 1.2  2000/03/10 19:00:18  matej
# Prvn��žÂ­ funkĂ��žÂn��žÂ­ verze, kter��ž��ž��ž um��žÂ­ pracovat tam i zpĂ��žÂt.
#
# Revision 1.1  2000/02/05 21:11:37  matej
# Initial revision
#
#
from sys import *
from os import *
from shutil import copyfile

def usage():
    print """Usage: w2i [-i|-l|-r|-w] filename
    Translate file filename from one to other Czech coding.
    -i from M$ CP1250 to UTF-8
    -a from UTF-8 to plain ASCII
    -l from PC Latin 2 to UTF-8
    -r from UTF-8 to ISO 8859-2
    -w from UTF-8 to M$ CP1250
    -u from ISO 8859-2 to UTF-8 (default)"""
    exit(1)

# system dependent -- create some individual tempfile
tempfile = "/tmp/w2i."+str(getpid())

smer_prekl = {'-l':["CP852","UTF-8"],\
              '-r':["UTF-8","ISO-8859-2"],'-w':["UTF-8","CP1250"],\
              '-i':["CP1250","UTF-8"],'-a':["UTF-8","ASCII"],
              '-u':["ISO-8859-2","UTF-8"]}

if len(argv) == 3:
    soubor = argv[2]
    parametry = smer_prekl[argv[1].lower()]
elif len(argv) == 2:
    soubor = argv[1]
    parametry = smer_prekl["-u"]
else:
    usage()

# system dependent -- run independent processor with
# processing results to variable result
#result = system("/usr/bin/cstocs " + parametry + " " + soubor + \
#                " > " + tempfile + " 2> /dev/null")
result = system("/usr/bin/iconv -c -f %s -t %s %s > %s 2> /dev/null" % \
	(parametry[0],parametry[1],soubor,tempfile))

# Depends on results of CSTOCS
if result == 0:
    # do proceed only when CSTOCS suceeded
    remove(soubor)
    copyfile(tempfile,soubor)
    remove(tempfile)
    # confirm to stderr, that everything is OK.
    print >>stderr,"OK."
else:
    # something's wrong -- delete tempfile and terminate script
    remove(tempfile)
    print >>stderr,"Something wrong happened -- exiting!"
    exit(1)
