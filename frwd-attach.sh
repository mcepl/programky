#!/bin/sh
# $Id$
# set -x

# How to read STDIN into variable
# unset input
# while read line; do input=${input}${line}$'\n'; done
# echo "$input" | razor-check && echo "Razor: Found Spam"
# echo "$input"
# or even better
# var=$(cat)

TO="$1"
CC="$2"
BCC="$3"
BODY="${4:-Forwarded message}"

tempfile=$(mktemp /tmp/$(basename $0)XXXXXX).eml
# copy message to temporary file
cat >${tempfile}

SUBJECT="FWD: $(grep '^Subject: ' $tempfile | head -1 | sed -e 's/^Subject: //')"

#echo "$BODY" | nail -s "$SUBJECT" -a "$tempfile" $TO
ref=$(dcop kmail KMailIface openComposer "$TO" "$CC" "$BCC" "$SUBJECT" "$BODY" 1)
# var aAMark = "addAttachment(QString,QCString,QByteArray,QCString,QCString,QCString,QString,QCString)";
# rec.call(aAMark,"bfproxy attachment","",stuff,"message","rfc822","","","");
dcop $ref addAttachment "file://$tempfile" "no comment"
dcop $ref send 1
rm -f $tempfile
unset tempfile
