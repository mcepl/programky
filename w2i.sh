#!/bin/sh
# $Log$
# Revision 1.4  2007/01/08 23:50:31  matej
# Different changes.
#
# Revision 1.3  2000/11/16 17:07:17  matej
# Remodelled to be of practial use.
#
# Revision 1.2  1999/11/25 17:29:16  matej
# Vyhozeno testovĂ��žnĂ­ paramter�šŻ �šĂ��ždku. Halt to neumĂ­m.
#

function usage() {
echo 'Usage: w2i [-i|-l|-r|-w] filename
Translate file filename from one to other Czech coding.

Paramter defines output coding:
-i ISO 8859-2 (default)
-a plain ASCII
-l PC Latin 2
-w M$ CP1250'
exit 1
}

# system dependent -- create some individual tempfile
TEMPFILE=$(mktemp /tmp/w2i.XXXXXX)

if [ $# -eq 2 ]
    then
    SOUBOR=$2
    case $1 in
	-i ) PARAMETRY="il2" ;;
	-a ) PARAMETRY="ascii" ;;
	-l ) PARAMETRY="852" ;;
	-w ) PARAMETRY="1250" ;;
    esac
elif [ $# -eq 1 ]
    then
    SOUBOR=$1
    PARAMETRY="il2"
else
    usage
fi

# system dependent -- run independent processor with
# processing results to variable result
tr2 `czenc --tr2 < $SOUBOR ` $PARAMETRY < $SOUBOR > $TEMPFILE 2> /dev/null

# Depends on results of TR2
if [ $? = "0" ]
    then
    # do proceed only when CSTOCS suceeded
    rm $SOUBOR
    cp $TEMPFILE $SOUBOR
    rm $TEMPFILE
    # confirm to stderr, that everything is OK.
    echo OK >&2
else
    # something's wrong -- delete tempfile and terminate script
    rm $TEMPFILE
    echo 'Something wrong happened -- exiting!' >&2
    exit 1
fi
