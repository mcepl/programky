#!/usr/bin/env python
"""
Library for manipulation with mail folders.

TODO:
   * Instead of one huge msglist, we should follow the original
     idea of mailbox module and create separate lists for each
     individual attribute of messages. Or should I get rid of
     mailbox module altgoether and rewrite it with my ideas in
     mind?
"""

from os import rename, remove as erase
from sys import stderr
from string import split, join
import mailbox
import mimetools

__author__ = 'Matej Cepl <matej@ceplovi.cz>'
__date__ = '$Date$'
__version__ = '$Revision$'

REPLIED = 1
SEEN = 2
TRASHED = 3
DRAFT = 4
FLAGGED = 5

statusmap = {"R": [REPLIED], "S": [SEEN], "T": [TRASHED],\
             "D": [DRAFT],"F": [FLAGGED]}

class Message(mimetools.Message):
   def __init__(self,fp,seekable=1):
      mimetools.Message.__init__(self,fp,seekable)

   def hasfromline(self):
      pass

class Maildir(mailbox.Maildir):
   """
   Class which works well with Maildir mailboxes.
   """
   def __init__(self,dirname,factory=Message):
      """
      Initialize attributes of the instance (esp. msglist).
      """
      mailbox.Maildir.__init__(self,dirname,factory)
      self.msglist=[]
      self.curmsg=0
      
      # scan mbox
      for msgind in range(len(self.boxes)):
         msgrec = {}
         msgrec["boxname"] = self.boxes[msgind]
         try:
            fp = open(msgrec["boxname"])
            msgrec["raw"] = self.factory(fp)
            fp.close()
         except:
            raise IOError
         msgrec["date"] = msgrec["raw"].getdate('date')
         msgrec["from"] = msgrec["raw"].get('from')
         msgrec["subject"] = msgrec["raw"].get('subject')
         msgrec["status"] = self.getstatus(msgrec["boxname"])
         self.msglist.append(msgrec)

   def next(self):
      """
      Return next message after the current and move pointer.
      """
      if not self.boxes:
          return None
      fp = open(self.boxes[self.curmsg])
      if self.curmsg < len(self.boxes)-1:
         self.curmsg += 1
         return self.factory(fp)
      else:
         return None

   def rewind(self):
      self.curmsg = 0

   def addflag(self,flag):
      """
      Add flag to the current message.
      """
      tmpmsg = self.msglist[self.curmsg]
      if flag not in tmpmsg["status"]:
         tmpmsg["status"] += flag
         oldname=tmpmsg["boxname"]
         sstring = self.getstatusstring(tmpmsg["boxname"])
         if len(sstring)>1:
            tmpstring = str(sstring[-1])+self.revstat(flag)
         else:
            tmpstring = self.revstat(flag)
         flags = list(tmpstring)
         flags.sort()
         if len(sstring)>1:
            tmpmsg["boxname"]=str(join(sstring[:-1]+[","]+flags,""))
         else:
            tmpmsg["boxname"]=str(join(sstring+[","]+flags,""))
         rename(oldname,tmpmsg["boxname"])
         self.msglist[self.curmsg] = tmpmsg 
   
   def delete(self):
      """
      Add 'trashed' flag to the current message. No actual
      deletion (expunging) of the message happens.
      """
      self.addflag([TRASHED])

   def expunge(self):
      """
      Expunge all messages marked as deleted from the mailbox.
      """
      tobedeleted = []
      ind = 0
      maxlen = len(self.msglist)
      while ind < maxlen:
         tmpmsg = self.msglist[ind]
         status = self.getstatus(tmpmsg["boxname"])
         if status and (TRASHED in status):
            erase(tmpmsg["boxname"])
            ind -=1
            maxlen -= 1
            del self.msglist[ind]
         ind +=1

   def revstat(self,flag):
      keys = statusmap.keys()
      for ind in keys:
         if flag==statusmap.get(ind):
            return ind
      return None

   def getstatusstring(self,boxname):
         found = split(boxname,",")
         if len(found)>1:
            return found
         else:
            return None

   def getstatus(self,boxname):
      string=self.getstatusstring(boxname)
      if string:
         status = []
         for chr in string[-1]:
            status += statusmap[chr]
         return status
      else:
         return None

if __name__ == "__main__":
   from os.path import expanduser
   mbx=Maildir(expanduser("~/mtest"))
   mbx.curmsg=10
   print mbx.msglist[mbx.curmsg]
   mbx.delete()
   print mbx.msglist[mbx.curmsg]
   print mbx.msglist[mbx.curmsg]
