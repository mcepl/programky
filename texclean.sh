#!/bin/sh
# $Log$
# Revision 1.7  2007/01/08 23:50:31  matej
# Different changes.
#
# Revision 1.6  2006-08-03 13:47:06  matej
# nĂ��žÂkter��ž�š  dal��žš��ž��ž��žÂ­ soubory m��žš�šť��žš�šžou j��žÂ­t
#
# Revision 1.5  2000/12/07 09:34:40  matej
# Corrected *.ld files removing.
#
# Revision 1.4  2000/10/30 00:29:34  matej
# Rewritten back to SH-script. Python is overkill for this.
#
# Revision 1.3  2000/10/07 18:44:07  matej
# Pridano jeste mazani souboru *.tid (produkuje makeindex)
#
# Revision 1.2  2000/06/06 17:42:59  matej
# Krom��ž�šźË�ťtoho, e smae tot��ž�šźË�ťco pedchoz��ž�šźË�ťBash verze, dok��ž�šźË�ť vymazat i
# .ld soubory u kterch existuje .lt soubor. Inu, s��ž�šźË�ťa Pythonu se
# nezape.
#
# Revision 1.1  2000/06/06 17:10:24  matej
# Initial revision

# standard (La)TeX stuff
rm -f *~ *.aux *.toc *.idx *.ind *.ilg *.glo *.bbl *.blg \
*.out *.tid *.brf *.gls
#*.log

# Metapost
rm -f *.mpx troff*.mpx mpx*

# when editing happened in M$-Windows
rm -f *.bak *.BAK

# .ctex is working file when vlnlatex (latex with vlna
# preprocessor) is in action
rm -f *.cstex

# this is usefull too
rm -f .*~

# well, next one is necessary, when kdvi is in action :-)
# to be exact -- kdvi behaves well, so we can comment it
# for future use :-(
rm -f core core.[0-9][0-9]*

# for texinfo
# *.fn is reserved
rm -f *.cp *.ky *.pg *.tp *.vr *.cps *.fns *.vrs *.kys

# vimoutliner
rm -f *.err vo_hoist* .vo_hoist*

# for endfloat
rm -f *.fff *.ttt

# for jurabib
rm -f *.url

# for tex4ht
rm -f *.4ct *.4tc *.idv *.lg *.tmp *.xref

# for LyX
rm -f *.lyx.emergency

# an idea?
#rcsclean *.lyx
