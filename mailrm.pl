#!/usr/bin/perl
#$Id$

@list = [];
$num = 0;
open INPIPE, "/usr/sbin/exim -bp|";
while (<INPIPE>) {
   chomp;
   split(' ');
   if ($#_ >= 3) {
      $num++;
      push @list,$_[2];
      print "$num : $_[2]\t$_[3]\n";
   } elsif ($#_ >= 0) {
      print "$_\n";
   }
}
close INPIPE;

if ($#list>0){
   print "Which message should be deleted: ";
   chomp($chr = <STDIN>);
   $len=length($chr);
   ($len > 0) or die "No answer.";
   die "Inproper answer" if (($chr < 0) or ($chr > $#list));

   system("/usr/sbin/exim -Mrm " . $list[$chr]);
}
