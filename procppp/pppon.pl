#!/usr/bin/perl
# $Id$

#use Strict;
use Getopt::Std;

$ppd_file='/var/tmp/ppp_connection.pid';
$optind=0;
$opt_s=0; $opt_q=0; $opt_h=0; $silent=0;
$ips = 'Surfbest';
$OK = 0;

sub usage {
die <<EOF;
Usage: $0 [-q|--silent] [-h|--help]
	Connect to the Internet via PPP.
	-q, --silent   Connect silently (good for working in the night :-)
	-h, --help     Print this (hopefully :-) helping information.
EOF
}

sub netstat {
	my $ison = 0;
	open(NETINFO,"/sbin/ifconfig|") || die "Can't get network information.!";
	while (<NETINFO>) {
		chomp;
		if (/^ppp|^eth/) {
			$ison = 1;
		}
	}
	close(NETINFO);
	return $ison;
}

# Do we have an Internet connection?
$result = &netstat;

# Process command line options
getopts('sqh');
$silent = ($opt_s == 1) || ($opt_s == 1) ? "Shh" : "Loud";

if ($result == 1) { $pid = 0; }
else {
	defined( my $pid = fork ) or die "Couldn't fork: $!";
	unless( $pid ) {
		exec "sudo -u root wvdial $ips $silent 2>&1 | logger -t wvdial &";
		die "Couldn't exec myscript: $!";
	}
}

open(PPDFILE,">$ppd_file");
print PPDFILE $pid;
print STDERR "$pid\n";
close(PPDFILE);

if ($pid == 0 } {exit 0;}

do  {
	$result = &netstat;
	sleep 5 ;
} until ($result == 1);

if ($> == 0) {
	system "chgrp staff $ppd_file; chmod a+r,og+w $ppd_file";
}
