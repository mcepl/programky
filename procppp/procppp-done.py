#!/usr/bin/env python
# $Id$
from gtk import *    # importing all the widgets from gtk
from gnome.ui import *
from os import system
from time import sleep

def nop(): # Doing nothing -- I forgot, how is called properly
    nopnop = 0

class Workhorse:                 # Problem domain
    def __init__(self):
        self.status = 0
        self.run_script()

    def run_script(self):
        nop()
        # self.status = system('pp')

    def run_mutt(self):
        self.status_mutt = system('mutt')

    def print_report(self):
        if self.status == 0:
            nop() # print "OK"
        else:
            print "Failed"

class Application(GnomeDialog):
    def __init__(self):
        GnomeDialog.__init__(self,'title')

class Application(GtkWindow): # User Interface
    def __init__(self):
         GtkWindow.__init__(self,WINDOW_TOPLEVEL)
         self.work = Workhorse()
         self.connect("destroy",self.destroy_cb)
         self.set_border_width = 3
         self.create_buttons()
    def create_buttons(self):
         table = GtkTable(2,2)
         self.add(table)
         Label = GtkLabel("The mail is being served.")
         table.attach(Label,0,1,0,1)
         OKButton = GnomeStockButton("OK")
         OKButton.connect("clicked",self.work_cb)
         table.attach(OKButton,0,1,1,1)
         MuttButton = GtkButton("Mutt")
         MuttButton.connect("clicked",self.work_mutt)
         table.attach(MuttButton,1,0,1,0)

    def work_cb(self,*args):
         self.work.print_report()
         self.destroy_cb()

    def work_mutt(self,*args):
         self.work.run_mutt()
         self.destroy_cb()

    def destroy_cb(self,*args):
         self.hide()
         mainquit()

if __name__=='__main__':
    app = Application()
    app.show_all()
    mainloop()

