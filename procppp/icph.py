#!/usr/bin/env python
# $Id$

import imaplib
import sys
import re
import os
import popen2
import getpass
import getopt
import string

imapuser="cepl.m"
imaphost='neumail04.neu.edu'
imapport=143
imappassword='messiah'
###imapuser="matej"
###imaphost='localhost'
###imapport=143
###imappassword='lubdkc'
imapinbox="INBOX"
# satest is what command is used test if the message is spam
satest="spamassassin --pipe --stop-at-threshold --exit-code >/dev/null"
# sasave is the one that dumps out a munged message including report
sasave="spamassassin --pipe"
# what we use to set flags on the original spam in imapbox
spamflagscmd="+FLAGS.SILENT"
# and the flags we set them to (none by default)
spamflags="("
# include the spamassassin report in the message placed in spaminbox
increport=1
# expunge before quiting causing all messages marked for deletion
# to actually be deleted
expunge=0
# print imap tracing info
verbose=0

# Usage message - note that not all options are documented
def usage():
   sys.stderr.write("""ICPH (IMAP CoPy Header):
Copies one header to another.""")
   sys.exit(1)

def errorexit(msg):
    sys.stderr.write(msg)
    sys.stderr.write("\nUse --help to see valid options and arguments\n")
    sys.exit(3)

# IMAP implementation detail
# Courier IMAP ignores uid fetches where more than a certain number are listed
# so we break them down into smaller groups of this size
uidfetchbatchsize=25

# This function checks that the return code is OK
# It also prints out what happened (which would end
# up /dev/null'ed in non-verbose mode)
def assertok(res,*args):
    if res[0]!="OK":
        sys.stderr.write("\n%s returned %s - aborting\n" % (`args`,  res ))
        sys.exit(2)
    if verbose:
        print `args`, "=", res

# Main code starts here
imap=imaplib.IMAP4(imaphost,imapport)

# Authenticate (only simple supported)
res=imap.login(imapuser, imappassword)
assertok(res, "login",imapuser, 'xxxxxxxx')

# select inbox
res=imap.select(imapinbox, 1)
assertok(res, 'select', imapinbox, 1)

# it returns number of messages in response
low=1
high=int(res[1][0])

# make the search
#uids=imap.search(None,'NEW','HEADER Mailing-List','NOT HEADER List-Post')
uids=imap.search(None,r'( NEW HEADER Mailing-List "" )')
#Mailing-List: list neu-lps-students@yahoogroups.com; \
#contact neu-lps-students-owner@yahoogroups.com
print uids

#@# uids=[]
#@# for i in possuids:
#@#     if i not in pastuids:
#@#         uids.append(i)
#@# 
#@# # for the uids we haven't seen before, get their sizes
#@# # The code originally got both the UIDs and size at the
#@# # same time.  This however took significantly longer as
#@# # I assume it stat()ed and perhaps even opened every message,
#@# # even the ones we had seen before
#@# sizeduids=getsizes(imap, uids)
#@# uids=[]
#@# for i in sizeduids:
#@#    uids.append(i[1])
#@# 
#@# # Keep track of new spam uids
#@# spamlist=[]
#@# 
#@# # Main loop that iterates over each new uid we haven't seen before
#@# for u in uids:
#@#     # Retrieve the entire message
#@#     res=imap.uid("FETCH", u, "(RFC822.HEADER)")
#@#     if res[0]!="OK":
#@#         assertok(res, 'uid fetch', u, '(RFC822)')
#@#     try:
#@#         body=res[1][0][1]
#@#     except:
#@#         if verbose:
#@#             print "Confused - rfc822 fetch gave "+`res`
#@#             print "The message was probably deleted while we are running"
#@#         pastuids.append(u)
#@# 
#@#     # Feed it to SpamAssassin in test mode
#@#     p=os.popen(satest, 'w')
#@#     p.write(body)
#@#     code=p.close()
#@# 
#@#     if code is None:
#@#         # Message is below threshold
#@#         pastuids.append(u)
#@#     else:
#@#         # Message is spam
#@#         if verbose: print u, "is spam"
#@#         spamlist.append(u)
#@# 
#@#         # do we want to include the spam report
#@#         if increport:
#@#             # filter it through sa
#@#             out,inp=popen2.popen2(sasave)
#@#             inp.write(body)
#@#             inp.close()
#@#             body=out.read()
#@#             out.close()
#@#             res=imap.append(spaminbox, None, None, body)
#@#             assertok(res, 'append', spaminbox, "{body}")
#@#         else:
#@#             # just copy it as is
#@#             res=imap.uid("COPY", u, spaminbox)
#@#             assertok(res, "uid copy", u, spaminbox)
#@# 
#@# # If we found any spams, now go and mark the original messages
#@# if len(spamlist):
#@#     res=imap.select(imapinbox)
#@#     assertok(res, 'select', imapinbox)
#@#     for u in spamlist:
#@#         res=imap.uid("STORE", u, spamflagscmd, spamflags)
#@#         assertok(res, "uid store", u, spamflagscmd, spamflags)
#@#         pastuids.append(u)
#@# 
#@# # only write out pastuids if it has changed
#@# if len(pastuids)!=pastuidslenorig:
#@#     f=open(pastuidsfile, "w+")
#@#     f.write("pastuids=")
#@#     f.write(`pastuids`)
#@#     f.write("\n")
#@#     f.close()
#@# 
#@# # only useful if we marked messages Deleted
#@# if expunge:
#@#     imap.expunge()
#@# 
#@# # sign off
#@# imap.logout()
#@# del imap
#@# 
#@# print "%d spams found in %d messages" % (len(spamlist), len(uids))
    
