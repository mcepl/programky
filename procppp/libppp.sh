# $Id$

SERVER=mitmanek
OUTSERVER=www.surfbest.net
FPING_WAITTIME=1000
#FPING_WAITTIME=500
SLEEPTIME=10

# constants for killspam
MFOLDER="$HOME/Maildir"
SPAMBOX="$HOME/.mail/junk"
#UNSUREBOX="$MFOLDER/qjunk"
MAILSOUND=/usr/local/share/sounds/gotmail/gospel-mail.wav

function check () {
   fping -t $FPING_WAITTIME -q $1 2>&1 >/dev/null
   #fping -q $1
   #fping -q $1 2>&1 >/dev/null
}

function logmsg()
{
   echo "$1" >> $2
}

function checkhome () {
  check $SERVER
  #grep -q 'eth0-home' /etc/network/ifstate 2>&1 >/dev/null
}

function newmails()
{
   # Tohle je skute��žn��ž vrchol -- nakopĂ­ruj pouze novĂŠ zprĂ��žvy, kterĂŠ
   # p�ši�š��žly od poslednĂ­ synchronizace
   if [ "$(ssh mitmanek ls $TMSTP_FILE)" != "" ]; then
      logmsg "---------------------------------------------------" $1
      logmsg "  KopĂ­ruj novĂŠ maily ze serveru" $1

      ssh mitmanek find ~/Maildir/new/ -type f -newer $TMSTP_FILE \
         | xargs -P 1 -i rsync -au mitmanek:'{}' ~/Maildir/new/
   fi
}

function smtpcheck () {
#	if /sbin/ifconfig | grep -q '^ppp' \
#		&& grep -q 'smtp.neu.edu' /etc/postfix/main.cf
#	then
#		smtp $MYISP
#	fi
echo "Function smtpcheck doesn't work" >&2
exit 999
}

function countout () {
     case $(( $1 % 5))$2 in
         0s) echo -n '*' ;;
         0n) echo -n ',' ;;
         0*) echo -n '#' ;;
         *s) echo -n '+' ;;
         *n) echo -n '.' ;;
         *) echo -n '?' ;;
     esac
}

function kmsg() {
    [ -z $DISPLAY ] || kdialog --msgbox $1
}

function msgcount() {
   MSGCOUNT=$(ls $1/ 2>/dev/null | wc -l | tr -d [:blank:])
}
