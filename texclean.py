#!/usr/bin/env python
# $Log$
# Revision 1.3  2000/10/07 18:44:07  matej
# Pridano jeste mazani souboru *.tid (produkuje makeindex)
# ,
#
# Revision 1.2  2000/06/06 17:42:59  matej
# Krom��ž toho, �šže sma�šže totĂŠ�šž co p�šedchozĂ­ Bash verze, dokĂ��ž�šže vymazat i
# .ld soubory u kterĂ�ťch existuje .lt soubor. Inu, sĂ­la Pythonu se
# nezap�še.
#
# Revision 1.1  2000/06/06 17:10:24  matej
# Initial revision

from os import remove
from os.path import splitext, isfile
from glob import glob

def delo(maska):
    for soubor in glob("./" + maska):
        remove (soubor)

delo("*~")
delo("*.aux")
delo("*.toc")
delo("*.idx")
delo("*.ind")
delo("*.ilg")
delo("*.glo")
delo("*.bbl")
delo("*.blg")
delo("*.out")
delo("*.log")
delo("*.tid")

# when editing happened in M$-Windows
delo("*.bak")
delo("*.BAK")

# well, next one is necessary, when kdvi is in action :-)
# to be exact -- kdvi behaves well, so we can comment it
# for future use :-(
delo("core")

# for texinfo
delo("*.fn")
delo("*.cp")
delo("*.ky")
delo("*.pg")
delo("*.tp")
delo("*.vr")

# to take care for lout's files
delo("*.ldx")
delo("*.li")
delo("*.lix")

# for every .lt remove appropriate .ld
for soubor in glob("./*.lt"):
    ldsoubor = splitext(soubor)[0]+".ld"
    if isfile(ldsoubor):
        remove (ldsoubor)
