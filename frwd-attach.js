#!/usr/bin/env kjscmd
// $Id$

var tempfile = "/tmp/frwd-attach"+Math.round(Math.random()*1e12);
var stuff = new String();
var stuffLine = new String();
var dcop = new DCOPClient(this);
var to = "";
var cc = "";
var bcc = "";
var body = "";
var subject = "";

if ( application.args.length == 0 ) {
    println("Usage:\n\tfrwd-attach TO CC BCC BODY");
    exit(1);
}
to = application.args[0];
if (application.args[1]) {
    cc = application.args[1];
}
if (application.args[2]) {
    bcc = application.args[2];
}
if (application.args[3]) {
    body = application.args[3];
} else {
    body = "Forwarded message.";
}

stuffLine = readLine();
while (stuffLine != null) {
    stuff += stuffLine + "\n";
    if ((stuffLine.search(/^Subject: /) != -1) && (subject == "")) {
        subject = stuffLine.replace(/^Subject: /,"");
    }
    try {
        stuffLine = System.readLine();
    } catch(e) {
//         println(e);
    }
}

exit(0);

dcop.attach();
if ( dcop.isAttached() ) {
    var rec = new DCOPRef(this);
    var fcMark = "openComposer(QString,QString,QString,QString,QString,bool)";
    var aAMark = "addAttachment(QString,QCString,QByteArray,QCString,QCString,QCString,QString,QCString)";
    rec = dcop.call("kmail","KMailIface",fcMark,to,cc,bcc,subject,body,false);
    println(rec);
    rec.call(aAMark,"bfproxy attachment","",stuff,"message","rfc822","","","");
    println(result);
//     rec.call("send(int)",1);
}