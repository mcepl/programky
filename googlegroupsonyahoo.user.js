// Google Groups on Yahoo Search
// $Id$
// 2005-05-26
// Copyright (c) 2005, Joe Grossberg,
// Copyright (c) 2005, Matej Cepl
// under Creative Commons SA 1.0
// http://creativecommons.org/licenses/sa/1.0/
//
// --------------------------------------------------------------------
//
// This is a Greasemonkey user script.
//
// To install, you need Greasemonkey: http://greasemonkey.mozdev.org/
// Then restart Firefox and revisit this script.
// Under Tools, there will be a new menu item to "Install User Script".
// Accept the default configuration and install.
//
// To uninstall, go to Tools/Manage User Scripts,
// select "Search Usenet from Yahoo!", and click Uninstall.
//
// --------------------------------------------------------------------
//
// ==UserScript==
// @name          Search Usenet from Yahoo!
// @namespace     http://www.ceplovi.cz/matej/progs/scripts
// @description   use Google Groups search on the Yahoo Search page
// @include       http://search.yahoo.com/*
// ==/UserScript==

(function() {
var contentString = String("");
var anchorTags = document.getElementsByTagName("a");
for (var i = 0; i < anchorTags.length ; i++)
{
	contentString = anchorTags[i].textContent.replace(/^\s*(.*)\s*$/,"$1");
	if (contentString == "&lt;&lt; Previous") {
		anchorTags[i].setAttribute("accesskey","P")
	} else if (contentString == "Next &gt;&gt;") {
		anchorTags[i].setAttribute("accesskey","N")
	};
}
  var gga = document.createElement("a");
  gga.setAttribute("href","http://groups.google.com/groups?oi=djq&as_q="+searchText);
  gga.appendChild(document.createTextNode("Usenet"));
  var aNodes = otherSearchesDiv.childNodes;
  var replaceNode=aNodes[aNodes.length-2];
  otherSearchesDiv.replaceChild(gga,replaceNode);
  var gmn = document.createElement("a");
  gmn.setAttribute("href","http://search.gmane.org/?query="+searchText+"&email=&group=&sort=relevance&DEFAULTOP=and&xFILTERS=--A");
  gmn.appendChild(document.createTextNode("Gmane"));
  otherSearchesDiv.appendChild(gmn);
})();