#!/usr/bin/python
"""
$Id$
Simple calculator for Linux console.
"""
from cmath import *
from math import atan2, ceil, fabs, floor, fmod, frexp, hypot, ldexp, modf, pow
from sys import *
from types import *
import readline

def get_expr():
    try:
        instring = str(raw_input('= '))
    except:
	print
	exit()
    return instring

expression = get_expr()
while expression != '':
    try:
        outstr=eval(expression)
    except SyntaxError:
	exit()
    except TypeError:
        pass
    else:
	pass
    if (type(outstr) is ComplexType) and (outstr.imag == 0):
	outstr=outstr.real
    print(str(outstr))
    outstr=''
    expression = get_expr()
exit()
