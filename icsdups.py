#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Usage: icsdups
filter -- get rid of the duplicate items in the ICS file from Korganizer.
"""
# $Id$

import sys,re

preamble = ""
preStat = True
icsObjStr = "V(EVENT|TODO|JOURNAL|FREEBUSY)"

rawStr = sys.stdin.read().strip()
rawList = re.split("BEGIN:%s" % icsObjStr,rawStr,1)
preamble = rawList[0]
rawList[1] = "BEGIN:V%s" % rawList[1]
rawStr = ("".join(rawList[1:]))
rawList = rawStr.split("\n\n")
# cut END:VCALENDAR -- nezapome� to p�idat na konec
rawList = rawList[:-1]

dataDict = {}
for rec in rawList:
	type=rec.split("\n")[0].split(":")[1]
	idkey=""
	for lin in rec.split("\n"):
		if type=="VEVENT":
			if lin.find("DTSTART")==0:
				idkey+=lin.split(":")[1]
		elif type=="VJOURNAL":
			if lin.find("LAST-MODIFIED")==0:
				idkey+=lin.split(":")[1]
			elif lin.find("DTSTAMP")==0:
				idkey+=lin.split(":")[1]
		elif type=="VTODO":
			if lin.find("LAST-MODIFIED")==0:
				idkey+=lin.split(":")[1]
			elif lin.find("DTSTAMP")==0:
				idkey+=lin.split(":")[1]
	if len(idkey)>0:
		dataDict[idkey]=(type,rec)

print preamble
for rec in dataDict:
	print "\n%s\n" % dataDict[rec][1]
print "\nEND:VCALENDAR\n"
