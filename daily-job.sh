#!/bin/sh
# $Id$
set -x

. /etc/profile
PATH=$HOME/bin:$PATH
TMPFILE=$(mktemp /tmp/daily-job.XXXXXX)

mailstat $HOME/log-procmail-matej \
	| mail matej@ceplovi.cz -s "Mailstat report (since 2004-10-29)"

# echo "Content-type: application/x-gzip; name=\"last-messages.gz\"" > $TMPFILE
# echo "Content-Transfer-Encoding: base64" >> $TMPFILE
# echo "Subject: List of yesterday messages" >> $TMPFILE
# echo "" >> $TMPFILE
# diff -u $HOME/log-procmail-matej.last $HOME/log-procmail-matej \
# 	| gzip -9Nc \
# 	| perl -MMIME::Base64 -0777 -ne 'print encode_base64($_)' >> $TMPFILE
# cp -f $HOME/log-procmail-matej $HOME/log-procmail-matej.last
# cat $TMPFILE | /usr/lib/sendmail matej@ceplovi.cz
# 
# rm -f $TMPFILE
# unset TMPFILE