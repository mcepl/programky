#!/usr/bin/perl -Wall
# $Id$

##set variables and functions
# I prefer to have all configuration in one place (and therefore
# I do not use relative dates in mutt).
$archdate="3 month ago";
$trashdate="1 fortnight ago";
$mdir="${\"HOME\"}/.mail";
$outdate="";

# archivation process itself
ARCHDIR="$HOME/archiv/$(date --date="$archdate" '+%Y')/mail";
AARCH=`date --date="$archdate" +%s`;

find $mdir/ -type d \
    -iregex $mdir.\*cur \
    \! -iregex $mdir/trash/cur \
    \! -iregex $mdir/inbox/cur \
    \! -iregex $mdir/_.\*/cur \
    \! -iregex $mdir/pratele/cur \
    \! -iregex $mdir/neu/cur \
    \! -iregex $mdir/work/cur \
    \! -iregex $mdir/spolvedy/cur \
    \! -iregex $mdir/software/cur \
    \! -iregex $mdir/sent.\* \
    \! -iregex $mdir/drafts.\* \
    -print | while read FOLDER; do
      FOLDER=${FOLDER::(${#FOLDER}-4)}
      WBOX=$(echo "$FOLDER" | sed -e "s:^$mdir/\.*\(.*\)$:\1:" \
         | tr [:upper:] [:lower:])
      WBOX=$(echo "$WBOX" | sed -e "s:\.directory::")
      ARCHBOX="$ARCHDIR/$WBOX"
      BASENDIR=$(dirname $ARCHBOX)
      ## if the directory and folder doesn't exist, create them
      [ -d $BASENDIR ] || mkdir -p $BASENDIR
      [ -f $ARCHBOX ] || touch $ARCHBOX
      echo -n "Processing $FOLDER "
      find $FOLDER -name \* -type f -name 1\* -print \
      | while read msg; do
         msgdate=$(date +%s -d "$(cat $msg | formail -c -x Date)")
         if [[ $msgdate < $AARCH ]]; then
            formail -ds < $msg >>$ARCHBOX
            rm $msg
            echo -n '.'
         fi
      done
      echo ''
    done

#move old sent-* files to archive
ARCHMONTH=$(date --date="$archdate" +%Y-%m);
find $mdir/ -type d -name sent-20\* -print \
   | while read WD; do
      FM=${WD:(-7)}
      if [[ "$FM" < "$ARCHMONTH" || "$FM" = "$ARCHMONTH" ]]; then
         $ndir="$ARCHDIR/sent-$FM"
         find $WD -name \* -type f -name 1\* -print \
         | while read msg; do
            formail -ds < $msg >>$NDIR
         done
         rm -rf $WD
      fi
   done
