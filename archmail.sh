#!/bin/bash
# $Id$
#set -x

##set variables and functions
# I prefer to have all configuration in one place (and therefore
# I do not use relative dates in mutt).
ARCHDATE="3 month ago"
TRASHDATE="1 fortnight ago"
MDIR="$HOME/.mail"
outdate=""

# archivation process itself
ARCHDIR="$HOME/archiv/$(date --date="$ARCHDATE" '+%Y')/mail"
AARCH=$(date --date="$ARCHDATE" +%s) 

find $MDIR/ -type d \
    -iregex $MDIR.\*cur \
    \! -iregex $MDIR/trash/cur \
    \! -iregex $MDIR/inbox/cur \
    \! -iregex $MDIR/_.\*/cur \
    \! -iregex $MDIR/pratele/cur \
    \! -iregex $MDIR/neu/cur \
    \! -iregex $MDIR/work/cur \
    \! -iregex $MDIR/spolvedy/cur \
    \! -iregex $MDIR/software/cur \
    \! -iregex $MDIR/sent.\* \
    \! -iregex $MDIR/drafts.\* \
    -print | while read FOLDER; do
      FOLDER=${FOLDER::(${#FOLDER}-4)}
      WBOX=$(echo "$FOLDER" | sed -e "s:^$MDIR/\.*\(.*\)$:\1:" \
         | tr [:upper:] [:lower:])
      WBOX=$(echo "$WBOX" | sed -e "s:\.directory::")
      ARCHBOX="$ARCHDIR/$WBOX"
      BASENDIR=$(dirname $ARCHBOX)
      ## if the directory and folder doesn't exist, create them
      [ -d $BASENDIR ] || mkdir -p $BASENDIR
      [ -f $ARCHBOX ] || touch $ARCHBOX
      echo -n "Processing $FOLDER "
      find $FOLDER -name \* -type f -name 1\* -print \
      | while read MSG; do
         MSGDATE=$(date +%s -d "$(cat $MSG | formail -c -x Date)")
         if [[ $MSGDATE < $AARCH ]]; then
            # pridelej nastaveni priznaku
            # Answered, Replied a Flagged.
            formail -ds < $MSG >>$ARCHBOX
            rm $MSG
            echo -n '.'
         fi
      done
      echo ''
    done

#move old sent-* files to archive
ARCHMONTH=$(date --date="$ARCHDATE" +%Y-%m)
find $MDIR/ -type d -name sent-20\* -print \
   | while read WD; do
      FM=${WD:(-7)}
      if [[ "$FM" < "$ARCHMONTH" || "$FM" = "$ARCHMONTH" ]]; then
         NDIR="$ARCHDIR/sent-$FM"
         find $WD -name \* -type f -name 1\* -print \
         | while read MSG; do
            formail -ds < $MSG >>$NDIR
         done
         rm -rf $WD
      fi
   done
