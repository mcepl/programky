#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""usage: isi2bibread [infile] [outfile]
"""
__version__="$Id$"
#by Phil Austin (phil@geog.ubc.ca)
#justify and write_bibtex_entry taken from Travis Oliphant's
#<Oliphant.Travis@mayo.edu> medtobib.py
# Substiantially rewritten by Matej Cepl (matej<at>ceplovi<dot>cz)

import sys,re,os

tagList = ("PT","AU","TI","DE","ID","AB","RP","CI","EM","TC",
    "NR","CR","CP","BP","EP","PG","DT","LA","SN","SO","J9","JI","SE","BS",
    "PY","PD","VL","IS","PN","SU","SI","GA","PU","PI","PA","WP")

deadWords = ("THE","A","AN","OF","ON","THIS","IS")

# tagnames from http://isibasic.com/help/helpprn.html
humanTags = {"FN":"File type",
"VR":"File format version number",
"PT":"Publication type (e.g., book, journal, book in series)",
"AU":"Author(s)",
"TI":"Article title",
"DE":"Author keywords",
"ID":"KeyWords Plus",
"AB":"Abstract",
"RP":"Reprint address",
"CI":"Research addresses",
"EM":"Authors' Internet e-mail address(es)",
"TC":"Times cited",
"NR":"Cited reference count",
"CR":"Cited references",
"CP":"Cited patent",
"BP":"Beginning page",
"EP":"Ending page",
"PG":"Page count",
"DT":"Document type",
"LA":"Language",
"SN":"ISSN",
"SO":"Full source title",
"J9":"29-character source title abbreviation",
"JI":"ISO source title abbreviation",
"SE":"Book series title",
"BS":"Book series subtitle",
"PY":"Publication year",
"PD":"Publication date",
"VL":"Volume",
"IS":"Issue",
"PN":"Part number",
"SU":"Supplement",
"SI":"Special issue",
"GA":"ISI document delivery number",
"PU":"Publisher",
"PI":"Publisher city",
"PA":"Publisher address",
"WP":"Publisher web address",
"ER":"End of record"}

bibtexTags = ("AU", # author
"TI", # title
"SO", # journal
"PY", # year
"PD", # date
"DE", # keywords
"EM", # email
"TC", # cited
"BP", # beginningPage
"EP", # endingPage
"LA", # language
"SN", # ISSN
"SE", # series
"VL", # volume
"IS", # issue
"PU", # publisher
"PI", # publisherCity
"PA", # publisherAddress
"AB", # abstract
)

tagString = "|".join(tagList)
tagRE = re.compile("(^|\n)\s*(%s)\s*" % tagString,re.MULTILINE)
linesRE = re.compile("\s*\n\s*",re.MULTILINE)
recSplitRE = re.compile("^\s*ER[\n\s]*",re.MULTILINE)

class Key:
    """Create record key according to
    http://www.nso.edu/general/computing/TeX/bibtex/bibshare
    which is same as $TEXMF/doc/bibtex/bibshare
    """
    def __init__(self,record):
        self.key = ""
        self.__inRecord = record
        self.__createKey()

    def __firstThreeLetters(self,inStr):
        if len(inStr)<1:
            raise ValueError,"no title"
        if type(inStr)==type([]):
            inStr = " ".join(inStr)
        # this is awkward, but I am too lazy to create
        # proper RE
        inStr = inStr.replace("_","")
        inStr = inStr.replace(" ","_")
        wrkStr = (re.sub("\W","",inStr)).split("_")
        outStr = ""
        i = -1
        while (len(outStr)<3):
            i += 1
            if len(wrkStr)>i:
                tmpWord = (wrkStr[i]).upper()
                if tmpWord in deadWords:
                    continue
                outStr += tmpWord[0]
            else:
                outStr += (wrkStr[0][i]).upper()
        return outStr

    def __createKey(self):
        tempKey = ""
        firstAuthorName = (self.__inRecord["AU"][0]).split(",")[0]
        tempKey += "%s:" % firstAuthorName.lower()
        if self.__inRecord["PT"] == "J":
            tempKey += "%s-" % self.__firstThreeLetters(self.__inRecord["SO"])
            tempKey += "%s-" % self.__inRecord["PY"]
            tempKey += "%s" % self.__inRecord["BP"]
        elif self.__inRecord["PT"] == "B":
            tempKey += "%s-" % self.__firstThreeLetters(self.__inRecord["TI"])
            tempKey += "%s" % self.__inRecord["PY"]
        else:
            raise IndexError,"Undefined type of publication %s,", \
                "please define it in Key.createKey()." \
                % self.__inRecord["PT"]
        if len(tempKey)>0:
            self.key = tempKey
        else:
            self.key = ""

class BibtexRecord:
    def __init__(self,inRecord,leftMargin=19,rightMargin=77,indent=3):
        self.__record = inRecord
        self.__lt=leftMargin
        self.__rt=rightMargin
        self.__id=indent
        # string representation of the BibTeX record
        self.__directlyHashable=("AB","DE","IS","LA","PA","PD","PI",\
            "PU","PY","SE","SN","SO","TC","TI","VL")
        self.__key2nameHash={"AB":"abstract","DE":"keywords",\
            "IS":"number","LA":"language","PA":"streetAddress","PD":"date",\
            "PI":"address","PU":"publisher","PY":"year","SE":"series",\
            "SN":"ISSN","SO":"journal","TC":"cited","TI":"title","VL":"volume"}
        self.bibRec = self.__createBibTeX()

    def __justify(self,big_string):
        # Split a large string into several lines of length (rt-lt+1)
        # at word boundaries. Add lt-1 spaces to beginning of all lines after
        # first one.
        words = big_string.split(' ')
        lines = []
        this_word = 0
        numwords = len(words)

        while this_word < numwords:
            current_line = " "*self.__id
            len_line = self.__id
            while 1:
                if this_word >= numwords:
                    break
                len_line = len_line + len(words[this_word]) + 1
                if len_line > (self.__rt - self.__lt + 2):
                    break
                current_line = current_line + words[this_word] + " "
                this_word = this_word + 1
            lines.append(current_line[:-1])
        return ("\n"+" "*(self.__lt-1)).join(lines)

    def __makeAuthors(self,authorList):
        # FIXME: this is just blurb
        if type(authorList)==type([]):
            return " and ".join(authorList)
        else:
            return authorList

    def __formatField(self,key):
        return self.__justify("%s = {%s},\n" % \
            (self.__key2nameHash[key],self.__record[key]))

    def __createBibTeX(self):
        if self.__record["PT"]=="J":
            outStr = "\n@article{%s,\n" % Key(self.__record).key
        elif self.__record["PT"]=="B":
            outStr = "\n@book{%s,\n" % Key(self.__record).key
        else:
            raise KeyError,"Unknown type of publication \"%s\"" % self.__record["PT"]
        usedKeys = [key for key in bibtexTags if key in self.__record.keys()]
        for key in usedKeys:
            if (type(self.__record[key])==type([]) and key not in ("AU","EM")):
                self.__record[key]=" ".join(self.__record[key])
            if key=="AU":
                tmpStr = "author = {%s}" % self.__makeAuthors(self.__record["AU"])
                outStr += "%s,\n" % self.__justify(tmpStr)
            elif key=="EM":
                if (type(self.__record["EM"])==type([])):
                    tmpStr = "email = {%s}" % (",".join(self.__record["EM"]))
                else:
                    tmpStr = "email = {%s}" % self.__record["EM"]
                outStr += "%s,\n" % self.__justify(tmpStr)
            elif key=="BP":
                if self.__record.has_key("EP"):
                    outStr += self.__justify("date = {%d-%d},\n" \
                        % (int(self.__record["BP"]),int(self.__record["EP"])))
                else:
                    outStr += self.__justify("date = {%d},\n" % int(self.__record["BP"]))
            elif key in self.__directlyHashable:
                outStr += self.__formatField(key)
        # cut out the last comma
        outStr = "%s\n}\n" % outStr[:-2]
        return outStr

class ParseRecord:
    def __init__(self,rawString):
        self.recordRec = {}
        self.currentTag = None
        self.fieldContent = ""
        self.parseRecord(rawString)

    def parseRecord(self,inStr):
        for field in tagRE.split(inStr):
            field = field.strip()
            if not(self.currentTag) and (len(field)==0):
                continue
            if field in tagList:
                if self.currentTag:
                    fieldRaw = linesRE.split(self.fieldContent.strip())
                    if len(fieldRaw)==1:
                        fieldRaw = fieldRaw[0]
                    self.recordRec[self.currentTag] = fieldRaw
                    self.fieldContent = ""
                self.currentTag = field
            else:
                if len(field)>0:
                    self.fieldContent = ("%s\n%s" % \
                        (self.fieldContent,field)).strip()

class ISIParser:
    def __init__(self,inf):
        self.infile = inf
        contStr = self.infile.read()
        self.infile.close()
        self.recList = self.parseFile(contStr)

    def parseFile(self,content):
        workList = []
        # skip over the header and mark of the end of file
        for record in recSplitRE.split(content)[1:-1]:
            rec = ParseRecord(record).recordRec
            workList.append(rec)
        return workList

class BibtexWriter:
    def __init__(self,recordList):
        self.__records=recordList
        self.out=self.__createOutput()

    def __createOutput(self):
        wrkStr = ""
        for record in self.__records:
            wrkStr += BibtexRecord(record).bibRec
        return wrkStr

if len(sys.argv)<2:
    inputFile = sys.stdin
else:
    inputFile = file(sys.argv[1],"r")

myFile = ISIParser(inputFile)

if len(sys.argv)<3:
    outputFile = sys.stdout
else:
    outputFile = file(sys.argv[2],"w")

print >>outputFile,BibtexWriter(myFile.recList).out

outputFile.close()

# kate: indent-mode python; replace-tabs-save on; replace-tabs on;