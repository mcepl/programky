#!/usr/bin/pybliographer
# $Id$
# This script fills references in XHTML file with citations from BibTeX database.
#
# Copyright (C) 2003 Matej Cepl
# Email : matej@ceplovi.cz
# Based on pybcompat from pybliographic
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# $Id$
#

import string, os, re, copy, sys
from Pyblio import Base, Key
from xml.sax import saxexts

False=None
True=not False

def usage ():
    print ("usage: pybfill <htmlfile> <bibtexfile>")
    sys.exit(0)

def error (msg):
    print >>sys.stderr, "pybfill: error: %s" % msg
    sys.exit (1)

# test input arguments
## testi = 0
## for argument in sys.argv:
##     print testi,argument
##     testi += 1
## sys.exit(0)
#########################
if len (sys.argv) < 4:
    usage ()
    sys.exit (1)

print >>sys.stderr, sys.argv
html  = sys.argv[2]
bibtex = sys.argv[3]
output = sys.stdout
print >>sys.stderr, "html = %s" % html
print >>sys.stderr, "bibtex = %s" % bibtex

# --- SAXtracer

class SAXtracer:

    def __init__(self,objname):
        self.objname=objname
        self.met_name=""
        self.processKey=False
        self.usedkeys = []

    def __getattr__(self,name):
        self.met_name=name # UGLY! :)
        return self.trace

    def error(self,exception):
        print >>sys.stderr, "err_handler.error(%s)" % str(exception)

    def fatalError(self,exception):
        print >>sys.stderr, "err_handler.fatalError(%s)" % str(exception)

    def warning(self,exception):
        print >>sys.stderr, "err_handler.warning(%s)" % str(exception)

    def characters(self,data,start,length):
        print >>sys.stderr, "doc_handler.characters(%s,%d,%d)" \
            % (`data[start:start+length]`,start,length)
    def startDocument(self):
        pass

    def endDocument(self):
        pass

    def characters(self,data,start,length):
        if self.processKey:
            keystr=data[start:start+length].encode('us-ascii')
            output.write(keystr+'">')
            # does the database provide the key ?
            key=Key.Key(db,keystr)
            if db.has_key(key):
                # yes, add it to the reference
                rec=db[key]
                if rec.has_key("author"):
                    authors=str(rec['author'])
                if rec.has_key("date"):
                    year=str(rec['date'])
                output.write('('+authors+','+year+')')
                self.usedkeys.append(key)
            else:
                raise ValueError,"Record "+keystr+" not found."
        else:
            output.write(data[start:start+length])

    def ignorableWhitespace(self,data,start,lengh):
        output.write(data[start:start+length])

    def startElement(self, name, attrs):
        """
        Only for cite elements print their content.
        """
        attr_str=""
        attr_dict={}
        for attr in attrs:
            attr_str='%s %s="%s"' % (attr_str,attr,attrs[attr])
        for attr in attrs:
            attr_dict[attr.encode('us-ascii')]=attrs[attr]

        if name=="cite":
            self.processKey=True
            output.write('<a class="PBFprocessed" key="')
        else:
            output.write('<'+name+attr_str+'>')

    def endElement(self,name):
        if name=="cite":
            self.processKey=False
            output.write('</a>')
        else:
            output.write('</'+name+'>')

    def trace(self,*rest):
        str="%s.%s(" % (self.objname,self.met_name)

        for param in rest[:-1]:
            str=str+`param`+", "

        if len(rest)>0:
            print str+`rest[-1]`+")"
        else:
            print str+")"

    def setDocumentLocator(self,*rest):
        pass

# use the bibliographic databases in order of declaration
# to solve the references

# open the database
db = bibopen(bibtex)
print dir(db.dict)
for item in db.dict:
   print item.key
   print type(db[item])
#    print db[item]

sys.exit(0)
# go for parsing XHTML
pf=saxexts.ParserFactory()
p=pf.make_parser("xml.sax.drivers.drv_xmlproc")

p.setDocumentHandler(SAXtracer("doc_handler"))
p.setDTDHandler(SAXtracer("dtd_handler"))
p.setErrorHandler(SAXtracer("err_handler"))
p.setEntityResolver(SAXtracer("ent_handler"))
p.parse(html)
