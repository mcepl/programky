### $Id$
## s = mat2tex (m,...)        - Convert a matrix to a latex array
##  
## INPUT :
## m     : N x M  : matrix
##
## OPTIONS :
## "align",  a : string : Alignement string for latex [lcr]. Default="l"
## "form" , f  : string : fprint format string               Default="%5.3f"
##
## OUTPUT :
## s     : string : Latex code that represents m
##
function s = mat2tex (m,...)

align = form = nan;
opt1 = " align format ";
nargin--;
read_options;

if isnan (align), align = "l"; end
if isnan (form) , form = "%5.3f"; end

C = columns (m);

if length (align) != C, align = align(rem (0:C-1, length(align))+1); end

s1 = sprintf ("\\begin{array}{%s}\n%%s\\end{array}\n",align);

				# Format for each row
f2 = [sprintf(" %s &",form)] * (kron(ones(1,C),1:length(form)+3));
f2 = [f2(1:length(f2)-1),"\\\\\n"];

s2 = sprintf (f2, m');

s = sprintf (s1, s2);

