#!/usr/bin/python
# -*- coding: iso-8859-2 -*-
"""Object for archiving messages from the Maildir folder into archive
mbox one."""

import email,email.Errors,email.Utils,mailbox
import sys,time,os

def msgfactory(fp):
   try:
      return email.message_from_file(fp)
   except email.Errors.MessageParseError:
      return ''

def write_maildir(msglist,origlist,dirname):
   """Practically, it means that all messages which are in origlist
   and not in msglist (which is a list of messages in the folder as it
   should be written) into the directory dirname. I should write sometime
   a full Maildir writer, but this is not the one yet."""
   newlist = [msg['fn'] for msg in msglist]
   dellist = [x for x in origlist if not(x in newlist)]
   addmsgs = [x for x in msglist if x['fn'] \
      in [x for x in newlist if not(x in origlist)]]
   if len(addmsgs)>0:
      raise ValueError,"""Some new messages should be stored in the mailbox,
      but it has not been implemented yet."""
   for file in dellist:
      os.remove(file)

class Mybox(mailbox.Maildir):
   def __init__(self,flddir,factory=msgfactory):
      """Create an object with the content according to the flddir Maildir
      directory."""
      self.box = mailbox.Maildir(flddir,factory)
      self.msglist = []
      sys.stderr.write("Loading folder %s: " % self.box.dirname)
      while True:
         msg = {}
         retval = self.next()
         if not retval:
            break
         msgorig,fn = retval
         msg['msg'] = msgorig
         msg['fn'] = fn
         sys.stderr.write('.')
         msg['date'] = email.Utils.parsedate_tz(msg['msg']['date'])
         self.msglist.append(msg)
      print >>sys.stderr,''
      self.oldlist = [msg['fn'] for msg in self.msglist]

   def next(self):
      if not self.box.boxes:
          return None
      fn = self.box.boxes.pop(0)
      fp = open(fn)
      return self.box.factory(fp),fn

   def archive(self,adate,fname):
      """Archive messages with date before adate from the parent folder
      into mbox folder with filename fname.
      Can I make separation into two lists with just one mapping command?"""
      alist = [x for x in self.msglist if x['date'] < adate]
      ffolder = file(fname,"a")
      for msg in alist:
         print >>ffolder,msg['msg']
      ffolder.close()
      self.msglist = [x for x in self.msglist if x['date'] >= adate]

   def flush(self):
      """Save the changed Maildir parent folder into the original
      directory."""
      write_maildir(self.msglist,self.oldlist,self.box.dirname)

if __name__=="__main__":
   box = Mybox("/home/matej/WP/trash/")
   archlist = list(time.localtime())
   archlist[1] -= 3
   archdate = tuple(archlist)
   print >>sys.stderr,"Archiving ..."
   box.archive(archdate,"/home/matej/outbox")
   print >>sys.stderr,"Flushing original folder ..."
   box.flush()
   print >>sys.stderr,"OK."