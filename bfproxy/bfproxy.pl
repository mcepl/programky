#!/usr/bin/perl -w
#
use strict;
use warnings;

# Used libraries -- where are they used?
use IO::Select;
use IPC::Open2;
use Fcntl qw(:flock);
use Benchmark;

#program version
my $VERSION="0.4";

#For CVS , use following line
#my $VERSION=sprintf("%d.%02d", q$Revision$ =~ /(\d+)\.(\d+)/);

# configuration constants
#?# our $path = "/bin:/usr/bin:/usr/local/bin";
# default shell
# our $shell = "/bin/sh";
our %cmdoptions  = ();
our $command = "";
our ($robx,$robs,$min_dev,$ham_cutoff,$spam_cutoff);

# maximum number of recursions for train-to-exhaustion
our $rmax = 10;

# get options from the command line
our %cmdoptions  = ();

# address-line command string (after the "+", before the "@")
our $command = "bfproxyID";

# delivery username; leave blank unless your MTA is unable
# to deliver to individual virtual host users but instead
# delivers to a single shared virtual host user
our $mailbox = "";  # most setups will not need this

sub proc_comm_line {
	getopts('hm:r:',\%cmdoptions);
	# address-line command string (after the "[+-]",
	# before the "@")
	$command = $cmdoptions{'r'} if defined $cmdoptions{'r'};
	$mailbox = $cmdoptions{'m'} if defined $cmdoptions{'m'};

	# delivery username; leave blank unless your MTA is unable
	# to deliver to individual virtual host users but instead
	# delivers to a single shared virtual host user
	our $mailbox = "";  # most setups will not need this
	$mailbox = $cmdoptions{'m'} if defined $cmdoptions{'m'};
}

sub set_environment {
	my $path = shift || "/bin:/usr/bin:/usr/local/bin";
	my $shell = shift || "/bin/sh";
	$> = $<; # set effective user ID to real UID
	$) = $(; # set effective group ID to real GID
	
	# Make %ENV safer
	delete @ENV{qw(IFS CDPATH ENV BASH_ENV PATH SHELL)};
	
	# Set the environment explicitely
	$ENV{PATH} = $path;
	$ENV{SHELL} = $shell;
}

sub get_config {
	my ($robx,$robs,$min_dev,$ham_cutoff,$spam_cutoff) = (0,0,0,0,0);
	open (CONF, "bogofilter -Q |") or error("warn","Could not get bogofilter settings");
	while (<CONF>) {
		chomp;
		my $line = $_;
		
		$robx = ($1+0) if $line =~ /robx\s*?=\s*?([0-9\.]+?)\s/i;
		$robs = ($1+0) if $line =~ /robs\s*?=\s*?([0-9\.]+?)\s/i;
		$min_dev = ($1+0) if $line =~ /min_dev\s*?=\s*?([0-9\.]+?)\s/i;
		$ham_cutoff = ($1+0) if $line =~ /ham_cutoff\s*?=\s*?([0-9\.]+?)\s/i;
		$spam_cutoff = ($1+0) if $line =~ /spam_cutoff\s*?=\s*?([0-9\.]+?)\s/i;
	}
	close CONF;

	return $robx, $robs, $min_dev, $ham_cutoff, $spam_cutoff;
}

################################################
################ Error Handling ################
################################################

sub error
{
	my ($action,$msg) = @_;

	die $msg if $action eq "die";
	warn $msg unless $action eq "die";
	# add other actions if you like
}

sub sig_trap
{
	my $sig = shift;
	my ($action,$more) = ("warn","");

	sig:
	{
			$action = "warn", last sig if $sig =~ /ALRM/;
			$action = "warn", last sig if $sig =~ /PIPE/;
			$action = "warn", last sig if $sig =~ /CHLD/;
			$action = "die" , last sig if $sig =~ /INT/;
			$action = "die" , last sig if $sig =~ /HUP/;
			$action = "warn";
	}

	my $waitedpid = wait;
	$more = "; Reaped pid $waitedpid, exited with status " . ($? >> 8) if $waitedpid;

	$SIG{$sig} = \&sig_trap;

	error ($action,"Trapped signal SIG$sig$more");
}

################################################
################# File Locking #################
################################################

sub assert_dominance {
	my ($handle,$type) = @_;

	# assert yourself
	unless (flock ($handle, $type)) {
		# get impatient
		sleep 3;

		# reassert yourself or give up
		unless (flock ($handle, $type)) { error ("die","File lock error: $!"); }
	}

	seek $handle, 0, 2;
}

sub learn_to_share {
	my ($handle) = @_;

	# give back to the community
	unless (flock($handle, LOCK_UN)) { error ("die","File unlock error: $!"); }
}

####### MAIN #########
&proc_comm_line;

if (defined $cmdoptions{'h'}) {
	my $bfproxy = $1 if $0 =~ /^([\w\/.\-~]*)$/;
	system("perldoc $bfproxy");
	exit(0);
}

($robx,$robs,$min_dev,$ham_cutoff,$spam_cutoff) = &get_config;
&set_environment;
# extract the header
my ($header,$boundary,$from,$options) = extract_header($command,$version,$mailbox);

# run bogofilter on the extracted emails and gather the results
my $results = process_emails($boundary,$from,$options);

# output the new email containing the results
print "$header\n$results\n";

__END__

=head1 SYNOPSIS

=head2 Command line usage:

B<bfproxy> [I<options>] < [I<rfc822_email>]

=head2 Procmail usage (recommended):

Add to ~/.procmailrc the following recipe.  This recipe needs to
be BEFORE the filtering recipe to prevent reclassifying all of
the forwarded messages again.

  :0fw
  * ^To: $USER\+bfproxyID
  | $HOME/.bogofilter/bfproxy

Where I<$HOME> is your home directory, if not set in the environment,
I<$USER> is your login name, if not set, and "I<bfproxyID>" is any
string by which you choose to identify mail that should be
processed by bfproxy.  Essentially, you are matching your regular
email address with "I<+bfproxyID>" after it. Eg:

  joe.shmoe+mybfproxystring@somedomain.com

Joe's procmail recipe would then be:

  :0fw
  * ^To: joe.shmoe\+mybfproxystring
  | $HOME/.bogofilter/bfproxy

Don't use the default "I<bfproxyID>" though, and keep yours
confidential so that people can't send bogofilter commands on
your behalf.  The from address would have to be spoofed, and
you'd get a report anyway, but why tempt fate?  Using your own
unique "I<bfproxyID>" helps prevent automated attacks by spammers
on bogofilter users. "I<bfproxyID>" is set either by direct
editing of the script (the old style), or with C<r> option to
the script (not in the address of the mail sent to the script,
but to the script itself).

When entering the bogofilter recipe after the bfproxy recipe, you
should add the C<E> flag so that bogofilter does not filter the
results of running bfproxy.  Eg:

  :0Efw
  | bogofilter -uep

It's not necessary, but suggested.  If you use the C<-u> option with
bogofilter, these bfproxy-trained emails would be trained again
otherwise, which is generally not what you want.  You would also
risk having the results thrown in with your spam, which would also
be undesireable.  Therefore, it's best to use the C<E> flag in the
bogofilter recipe.

Just remember to make sure that the bogofilter recipe is AFTER the
bfproxy recipe to prevent screwing up your database by double-
classifying those attached emails.

=head2 Email usage:

When using an MDA such as Procmail to handle running bfproxy, as
intended, you'll need to send yourself emails with the special
"I<bfproxyID>" included, followed by any options. Eg:

[I<USER>]+[I<bfproxyID>]-[I<options>]@[I<DOMAIN>]

The email you send to this address should contain any emails you
want to correct as attachments.  Using your email client's forward-
as-attachment functionality should suffice.  All such attached
emails will be processed according to address-line options.

=head1 OPTIONS:

=head2 Address-line options:

=over 4

=item B<C>

all emails with an original bogosity of I<spam> or I<ham> will
be unregistered and then re-registered as the opposite bogosity
(C<-Sn>, C<-Ns>).  This is useful if the emails were originally
auto registered with the C<-u> flag or if a user error occurred.

=item B<R>

all emails with an original bogosity of I<spam> or I<ham> will
be registered as the opposite bogosity (C<-n>, C<-s>).

=item B<N>

unregister all attached emails from the ham database

=item B<S>

unregister all attached emails from the spam database

=item B<n>

register all attached emails as ham

=item B<s>

register all attached emails as spam

=item B<x>

Repeatedly register emails that are within the "unsure" zone
of spamicity values (between ham_cutoff and spam_cutoff) until
they are correctly classified or until I<rmax> has been reached.

=item B<v>

Verbose output.  Show per-email registration information in
addition to the subject lines and summary.

=item B<b>

Show benchmarking information in the output.

=item B<c>

Display bogofilter configuration info in the output.

=back


If no address-line command is provided, it defaults to C<C> for
"correction" mode.  If you choose to use C<C> or C<R> and wish to
register unsures at the same time, you can add a secondary command
after the C<C> or C<R> to register unsures as either spam (C<s>)
or ham (C<n>).  Eg:

	joe.shmoe+mybfproxystring-Rs@somedomain.com

The result of the above command string will be that all emails
previously classified as spams will be registered as hams, all
emails previously classified as hams will be registered as spams,
and all emails previously classified as unsures will be registered
as spams.  If a secondary command is not used with C<C> or C<R>,
then unsures are not registered.

=head2 Command-line options:

=over 4

=item B<h>

display this help file

=item B<r>

set address line command string (after the "+", before the
actuall commands send to bfproxy).

=item B<m>

delivery username; this is for MTAs which deliver to a single
shared virtual host user (e.g., qmail in the standard
configuration).

=back

=head1 REQUIRES

=over 4

=item *

Perl 5.6.1

=item *

bogofilter

=back

=head1 DESCRIPTION

Bfproxy accepts an email on stdin (generally from procmail or some
other MDA) containing one or more forwarded-as-attachment emails.
It will extract the attached emails and remove [SPAM] indicators
from the subject line.  Then, it'll determine the original
classification of each email from its X-Bogosity field, and pass
the original emails to bogofilter with the appropriate flags as
determined by address-line options.

The attachments containing the emails should be message/rfc822
format, which is how most email clients will do
forwarding-by-attachment by default.  If they are not in this
format, then it is quite likely that important header information
is being omitted, and bfproxy will ignore such attachments.
Moreover, bfproxy will only recognize attachments in encapsulating
MIME types multipart/mixed or multipart/digest.  Quoted-text
attachments will not work, nor will inline attachments which do not
conform to the previous requirements (rfc822 and mixed or digest).
Just about any MUA should be able to meet these requirements for
forwarding emails.  If there is one that uses a slightly different,
but possibly acceptable format, please send a bug report to
neo+bfproxy@orderamidchaos.com.

Also, bfproxy will not parse attached emails multiple MIME levels.
If an attachment has an attachment, then the whole thing will get
parsed as a single email rather than accounting for the headers in
the attached email.  Do not nest corrections within each other.

This scheme uses "subaddressing" to direct the MDA to run the
email through bfproxy without requiring a new user or alias on the
system.  Moreover, the output of the operation will arrive in the
user's mailbox without having to resend a second email.  This also
allows bfproxy to run with the permissions of the user whose
database is to be altered by the command, and access his/her
environment, including the appropriate config files and db files.
See RFC3598 (http://www.ietf.org/) for details on "subaddressing".
It is supported by common MTA's such as Sendmail.

=head1 FAQ

=head2 Why isn't it working?

If you get an error the first time you try it, such as bfproxy
saying it received zero emails for correction, then view the source
of the email you sent and make sure the attachments use
message/rfc822 format, and that the attachments are contained in a
MIME part of multipart/mixed or multipart/digest.  If not, check your
email client's options to see if you can get it into that format.  If
you get any other errors/problems, please submit a bug report.
Another problem my be in not setting "I<bfproxyID>" same way in
the script (or with C<r> option on the command line) and in
reality (usually in procmail recipe).

=head2 Why call it 'bfproxy' instead of 'bogoproxy', etc.?

Because "bogo" refers to bogons which are the elementary particle
of bogosity which bogofilter filters.  We're not proxying bogons
but commands to bogofilter.  Therefore, despite the lyrical
synergy that "bogoproxy" would have, it simply wouldn't make any
logical sense, whereas the less elegant bfproxy is more
descriptive of its actual function... proxying "bf" commands.

=head2 What happens to X-headers from my client/proxy/etc?

X-headers are left as-is now, unlike initial bfproxy behavior.  The
reason is that bogofilter's Bayesian algorithm will essentially make
those X-headers irrelevant to classifications since an equal number
will come from spams as from hams.  Therefore emails will end up
being classified based on other tokens instead.  And this way,
X-headers from senders' clients/proxies/servers/etc may be useful in
classifying.  Furthermore, this reduces the number of ad hoc rules
that bfproxy applies and would need to be maintained.  Bogofilter
already strips the X-Bogosity header, so it is unnecessary to do so
in bfproxy.

=head1 BUGS

=over 4

=item Please report any.

=back

=head1 TODO

=over 4

=item Add more verbosity options

=item Use bogofilter -Q to obtain non-standard tags, formats, etc.

=item Suggestions welcome.

=back

=head1 SEE ALSO

L<bogofilter>

=head1 AUTHOR

Tom Anderson <neo+bfproxy@orderamidchaos.com>

=cut

# kate: tab-width 4; indent-width 4; replace-tabs off; eol unix;