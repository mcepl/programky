package ContentMessage;

use strict;

require Message;
use vars       qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

# set the version for version checking
$VERSION     = 0.4;

@ISA         = qw(Exporter);
@EXPORT      = qw(&new &process_emails &train &bogofilter);
%EXPORT_TAGS = ( );     # eg: TAG => [ qw!name1 name2! ],

# your exported package globals go here,
# as well as any optionally exported functions
# @EXPORT_OK   = qw($Var1 %Hashit &func3);

# use vars qw($Var1 %Hashit);
# non-exported package globals go here
# use vars qw(@more $stuff);

# initialize package globals, first exported ones
$Var1   = '';
%Hashit = ();

# then the others (which are still accessible as $Some::Module::stuff)
$stuff  = '';
@more   = ();


# file-private lexicals go here
my $priv_var    = '';
my %secret_hash = ();


# sub train
# sub bogofilter

sub new {
	my ($class) = @_;
	my $self = {};
	# my $self =
	#    { _street => undef, _city => undef, _state => undef, _zip => undef };
	bless $self, $class;
	return $self;
}

sub process_emails {
	# 1) Read the email from STDIN
	# 2) Discard the enveloping email and process only
	#    the "message/rfc822" MIME parts
	#    representing the forwarded-as-attachment incorrectly
	#    classified emails
	# 3) Send emails to bogofilter for registration
	# 4) Output the results

	my $self = shift;
	my $boundary=shift||"";	# multipart boundary
	my $from = shift || "";	# address of sender
	my $options=shift ||"";	# address-line flags

	my @count = (0,0,0,0);	# count of emails
							# (found, processed, lines, words)
	my $locked = 0;			# lock the boundary at the shallowest
							# level where rfc822 content found
	my %email;				# hash to temporarily hold a single email
	my $found = 0;          # indicator of when to record an email
							# 0: no rfc822 messages found yet
							# 1: we've got a message/rfc822 header
							# 2: we're out of the header part

	# start timing the process
	my $start_time = new Benchmark if $options =~ /b/;

	# begin generating output
	my $results .= "Bfproxy has processed the following emails with option $options:\n\n";

	while (<STDIN>) {
		chomp;
		my $line = $_;
		$count[2]++;

		# try to find any new attached emails
		if ($found < 2) {
			# record the boundary of the multipart/mixed or digest MIME
			# message
			unless ($locked) {
				$boundary = $1 if 
					$line =~ /Content-Type: multipart\/(?:mixed|digest); boundary="(.*?)"/i; }
			# we've found a new attached email if the content-type is message/rfc822 and we
			# have a boundary
			if ($line =~ /Content-Type: message\/rfc822/i && $boundary) {
				$found++;
				$locked = 1; # lock the boundary on first attachment
			}

			# we're ready to start recording if we're out of the attachment
			# header (found a blank line)
			$found++ if ($line !~ /[^\s]/i && $found == 1);
		}

		# if we're inside an attached email, record it
		else {
			if ($line !~ /\Q$boundary\E/) {
				# strip [SPAM] token from subject
				$line =~ s/^Subject: (.*?)\[(?:SPAM|UNSURE)\]/Subject: $1/i;
	
				# escape From lines in body
				$line =~ s/^(From) />$1 /i;
	
				# record properties of this email
				$email{'spam'} = 2 if $line =~ /^X-Bogosity: (?:Unsure)/i;
				$email{'spam'} = 1 if $line =~ /^X-Bogosity: (?:Yes|Spam)/i;
				$email{'spam'} = 0 if $line =~ /^X-Bogosity: (?:No|Ham|Clean)/i;
	
				$email{'spamicity'} = $1 if $line =~ /spamicity=((?:\d|\.)*?),/;
				$email{'content'} .= "$line" unless $line =~ /^X-[^\s]*?:/i;
				$email{'return-path'} = $1 if $line =~ /^Return-Path: <?(.*?)(?=>|\n)/i;
				$email{'from'} = $1 if !defined $email{'from'} &&
					$line =~ /^From: (?:.*?<)?(.*?)(?=>|\n)/i;
				$email{'subject'} = $1 if $line =~ /^Subject: (.{0,40})(.*?)/i;
				$email{'subject'} .= "..." if $line =~ /^Subject: .{40}.+?/i;
			} else {
				$count[0]++;
				$email{'subject'} = "No Subject" unless $email{'subject'};
				$email{'spam'} = 2 unless defined $email{'spam'};
				$email{'spamicity'} = "None" unless $email{'spamicity'};
	
				my $training = $self->train(\%email,$options,0,\@count);
	
				# per-email output
				$results .= "subject: $email{'subject'}\n";
				$results .= "original spamicity: $email{'spamicity'}\n" . \
					$training if $options =~ /v/;
	
				# close this email and advance to the next
				undef %email;
				$found = 0;
			}
		}
	}

	$results .= "\n";
	$results .= "$count[0] emails found, containing $count[2] lines total.\n";
	$results .= "$count[3] words from $count[1] emails were registered.\n\n";

	if ($options =~ /b/) {
		# calculate total running time
		my $end_time = new Benchmark;
		my $td = timediff($end_time, $start_time);
		my $cpu = $td->[1]+$td->[2]+$td->[3]+$td->[4];
		my $wall = $td->[0];
		my $per_line = $cpu / $count[2];
			$per_line = int(($per_line*1000) + .5 * ($per_line <=> 0)) / 1000;
		my $per_mail = $cpu / $count[0];
			$per_mail = int(($per_mail*1000) + .5 * ($per_mail <=> 0)) / 1000;

		$results .= "Total running time was $wall wallclock secs, $cpu CPU secs.\n";
		$results .= "$per_line CPU secs/line, $per_mail CPU secs/email.\n";
		$results .= "Bfproxy required ".$td->[1].\
			" usr + ".$td->[2]." sys = ".($td->[1]+$td->[2]).\
			" CPU secs.\n";
		$results .= "Bogofilter required ".$td->[3]." usr + ".\
			$td->[4]." sys = ".($td->[3]+$td->[4])." CPU secs.\n\n";
	}

	if ($options =~ /c/) {
		$results .= "robx=$robx, robs=$robs, min_dev=$min_dev, " .\
			"spam_cutoff=$spam_cutoff, ham_cutoff=$ham_cutoff, rmax=$rmax\n";
	}

	return $results;
}

################################################
############### Perform Training ###############
################################################

sub train
{
	my $self = shift;
	my $email = shift or error("warn","No email provided");
	my $options = shift || "C";
	my $r = shift || 0; # recursion counter for exhaustive training
	my $count = shift || (0,0,0,0);
	my $results = "";

	# if action is corrective, check the bogosity to decide what to do
	if ($options =~ /[CR]/) {
		# email was classified as SPAM
		if ($email->{'spam'} == 1) {
			my $flag = ($options =~ /C/)? "Sn" : "n";
			$results .= "user classification: ham\ncommand: bogofilter -${\$flag}\n" unless $r;
	
			my ($status,$words,$output) = bogofilter("-${\$flag}evD 2>&1",$email->{'content'});
			unless ($status || $r) {
				$count->[1]++;
				$count->[3]+=$words;
				$results .= "words: $words\n";
			}
			elsif ($status) {$results .= "could not process this email: $status\n";}
		}
		# email was classified as HAM
		elsif ($email->{'spam'} == 0) {
			my $flag = ($options =~ /C/)? "Ns" : "s";
			$results .= "user classification: spam\ncommand: bogofilter -${\$flag}\n" unless $r;
			my ($status,$words,$output) = bogofilter("-${\$flag}evD 2>&1",$email->{'content'});
			unless ($status || $r) {
				$count->[1]++;
				$count->[3]+=$words;
				$results .= "words: $words\n";
			}
			elsif ($status) {$results .= "could not process this email: $status\n";}
		}
		# email was classified as UNSURE
		elsif ($email->{'spam'} == 2) {
			my $new_options = $options; $new_options =~ s/[CR]//gs;
			$results .= $self->train($email,$new_options,$r,$count);
			return $results;
		}
	}
	# if action is direct, just do it
	elsif ($options =~ /[NSns]/) {
		my $flag = $options; $flag =~ s/[^NSns]//gs;
		my $class = ($flag =~ /s/)? "spam" : ($flag =~ /n/)? "ham" : "none";
		$results .= "user classification: $class\ncommand: bogofilter -${\$flag}\n" unless $r;
		my ($status,$words,$output) = bogofilter("-${\$flag}evD 2>&1",$email->{'content'});
		unless ($status || $r) {
			$count->[1]++;
			$count->[3]+=$words;
			$results .= "words: $words\n";
		}
		elsif ($status) { $results .= "could not process this email: $status\n"; }
	}
	else {
		$results .= "could not process this email: no option passed in for this condition\n";
	}

	# check new spamicity
	my ($status,$words,$output) = bogofilter("-te 2>&1",$email->{'content'});
	unless ($status) {
		$output =~ s/^.*?((?:\d|\.)*?)$/$1/;
		$results .= "new spamicity: $output"; }
	else {
		$results .= "could not classify this email: $status\n";
	}

	# train-to-exhaustion
	$options =~ s/[^nsx]//gis; # only do registration on subsequent recursions
	$results .= $self->train($email,$options,$r+1,$count) \
		if $options =~ /x/ && $r < $rmax && (($options =~ /s/ && $output < $spam_cutoff) \
		|| ($options =~ /n/ && $output > $ham_cutoff));

	$results .= "\n" unless $r;

	return $results;
}

################################################
################ Run Bogofilter ################
################################################

sub bogofilter
{
	my $self = shift;
	my $options = shift || "";
	my $email = shift || "";
	my $status = 0;
	my $output = "";
	my $words = "";

	# for environments that don't split virtual host users into system users
	$options = "-d $ENV{HOME}/.bogofilter/$mailbox " . $options if $mailbox;

	# trap broken pipes
	$SIG{PIPE} = \&sig_trap;

	# fork pipe to bogofilter
	my $pid = open2(\*R,\*W,"bogofilter $options") or
		error("die","Could not open pipe to bogofilter: $!");

	# lock filehandle
	assert_dominance (\*W,LOCK_EX);

	# create filehandle references
	my $R = *R{IO};   my $W = *W{IO};

	# create select object and add handles
	my $sel = IO::Select->new($R,$W) or error("die","Cannot create IO::Select object: $!");

	# set autoflush
	select((select(W), $| = 1)[0]);

	# select strings
	my $ex = join(',', $sel->has_exception(0));
	my $cw = join(',', $sel->can_write(0));
	my $w  = "$W";

	# send email to bogofilter
	# unless there was an exception on this filehandle
	unless ($ex =~ /\Q$w\E/) {
		# if this filehandle is writeable
		if ($cw =~ /\Q$w\E/) {
			syswrite W, $email \
				or error("die","Cannot write to pipe: bogofilter $options: $email: $!");
		}
		else {error("warn","Filehandle $w is not in ready list: $cw: $!");}
	}
	else { error("warn","Filehandle $w had an exception: $!"); }

	# close write filehandle to flush the buffer and read from the process outputs
	if (close W) {
		$status = $? >> 8;  # exit status
		sysread R, $output, 32 or error("warn","Cannot read from pipe: $!");
		$words = $1 if $output =~ /^#\s*?(\d*?) words.*/;
	}
	else { error("warn","Could not flush output to bogofilter: $!"); }

	# close read filehandle
	unless (close R) {error("warn","Could not close input from bogofilter: $!");}

	# terminate child processes
	waitpid $pid, 0;

	return $status, $words, $output;
}



END { }       # module clean-up code here (global destructor)

1;

__END__

=head1 NAME

ModuleName - short discription of your program

=head1 SYNOPSIS

how to us your module

=head1 DESCRIPTION

long description of your module

=head1 SEE ALSO

need to know things before somebody uses your program

=head1 AUTHOR

Matej Cepl

=cut

# kate: tab-width 4; indent-width 4; replace-tabs off; eol unix;