package EnvelopeMessage;

use strict;

require Exporter;
use vars       qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

# set the version for version checking
$VERSION     = 0.4;

@ISA         = qw(Exporter);
@EXPORT      = qw(&hello);
%EXPORT_TAGS = ( );     # eg: TAG => [ qw!name1 name2! ],

# your exported package globals go here,
# as well as any optionally exported functions
@EXPORT_OK   = qw($Var1 %Hashit &func3);

use vars qw($Var1 %Hashit);
# non-exported package globals go here
use vars qw(@more $stuff);

# initialize package globals, first exported ones
$Var1   = '';
%Hashit = ();

# then the others (which are still accessible as $Some::Module::stuff)
$stuff  = '';
@more   = ();


# file-private lexicals go here
my $priv_var    = '';
my %secret_hash = ();


sub new {
    my ($class) = @_;
	my $self = {};
	# my $self =
	#    { _street => undef, _city => undef, _state => undef, _zip => undef };
    bless $self, $class;
    return $self;
}

sub extract_header {
	my $command = shift;
	my $version = shift;
	my $user = shift||"";
	my $header = "";
	my $boundary = "";
	my $multipart = 0;
	my $from = "";
	my $to = "";

	while (<STDIN>) {
	  chomp;
	  my $line = $_;

	  # record the boundary of the multipart/mixed or digest MIME message
	  $multipart = 1 if $line =~ /Content-Type: multipart\/(?:mixed|digest);/i;
	  $boundary = $1 if $multipart && $line =~ /boundary="?([^"]*)"?/i;
	  # replace the from, subject, and content-type lines in the headers
	  $from = $1 if $line =~ s/^From:\s(.*)$/From: Bfproxy v$version/i;
	  $line =~ s/^Subject:\s.*$/Subject: training results/i;
	  $line =~ s/^Content-type:\s.*$/Content-Type: text\/plain/i;
	  $to = $1 if $line =~ /^To:\s(.*)$/i;

	  # I am totally confused from the following ... :-(
	  # $header .= $line; # if $line =~ /^\w*?:\s|^From\s/i; # unless $line =~ /^X-[^\s]*?:/i;
	  # $header = $line if $line =~ /^From\s/i; # unless $line =~ /^X-[^\s]*?:/i;
	  $header .= "$line\n";

	  # we're done with the header when we've found a blank line
	  last if $line !~ /[^\s]/i;
	}

	# extract the "user" portion of the "from" address
	# only when $mailbox is not defined
	if ($user eq "") {
		$user = $1 if $from =~ /.*?((?:\w|-|\.)+?)\@.*$/i;
	}

	# parse any address-line flags
	my $options = ($to =~ /.*?$user[+-]$command\-(.*?)\@.*$/i)? $1 : "";

	return $header, $boundary, $from, $options;
}



END { }       # module clean-up code here (global destructor)

1;

__END__

=head1 NAME

ModuleName - short discription of your program

=head1 SYNOPSIS

 how to us your module

=head1 DESCRIPTION

 long description of your module

=head1 SEE ALSO

 need to know things before somebody uses your program

=head1 AUTHOR

 Matej Cepl

=cut

# kate: tab-width 4; indent-width 4; replace-tabs off; eol unix;