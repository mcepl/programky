#% Do not delete the line below; configure depends on this
#  \DeclareLaTeXClass[fldnote]{field notes}
# Matej Cepl <matej@ceplovi.cz>

# General textclass parameters
Input scrclass.inc
MaxCounter              Counter_Section
NoStyle Chapter
NoStyle Chapter*
NoStyle Addchap
NoStyle Addchap*
NoStyle Part
NoStyle Part*
NoStyle Title
SecNumDepth             3
TocDepth                3

Preamble
\setlength{\overfullrule}{5pt}
\deffootnote{1em}{1em}%
        {\textsuperscript{\normalfont\thefootnotemark\ }}
EndPreamble

# Modify Date
Style Date
   LatexType      Command
   LatexName      date
   LeftMargin		Date:xx
   LabelSep       xx
   LabelType      Static
   LabelString    Date:
   InTitle        1
   Align          right
   AlignPossible  right
   Font
      Size        normal
   EndFont
   LabelFont
      Color       magenta
   EndFont
End

Style Where
   LatexType      Command
   LatexName      where
   LeftMargin		Where:xx
   LabelSep       xx
   LabelType      Static
   LabelString    Where:
   InTitle        1
   LabelFont
      Color       magenta
   EndFont
End

Style Who
   LatexType      Command
   LatexName      who
   LeftMargin		Who:xx
   LabelSep       xx
   LabelType      Static
   LabelString    Who:
   InTitle        1
   LabelFont
      Color       magenta
   EndFont
End

Style Institution
   LatexType      Command
   LatexName      institution
   LeftMargin		Inst.:xx
   LabelSep       xx
   LabelType      Static
   LabelString    Inst.:
   InTitle        1
   LabelFont
      Color       magenta
   EndFont
End

Style Phone
   LatexType      Command
   LatexName      phone
   LeftMargin		Phone:xx
   LabelSep       xx
   LabelType      Static
   LabelString    Phone:
   InTitle        1
   LabelFont
      Color       magenta
   EndFont
End

Style Position
   LatexType      Command
   LatexName      position
   LeftMargin		Position:xx
   LabelSep       xx
   LabelType      Static
   LabelString    Position:
   InTitle        1
   LabelFont
      Color       magenta
   EndFont
End

Style Email
   LatexType      Command
   LatexName      email
   LeftMargin		xEmail:xx
   LabelSep       x
   LabelType      Static
   LabelString    Email:
   InTitle        1
   Font
      Family      Typewriter
   EndFont
   LabelFont
      Color       blue
   EndFont
End

Style Address
   LatexType      Command
   LatexName      address
   LeftMargin		Addr.:xx
   LabelSep       xx
   LabelType      Static
   LabelString    Addr.:
   InTitle        1
   BottomSep      0
   LabelFont
      Color       magenta
   EndFont
End

Style Where
   LatexType      Command
   LatexName      address
   LeftMargin		Where:xx
   LabelSep       xx
   LabelType      Static
   LabelString    Where:
   InTitle        1
   BottomSep      0
   LabelFont
      Color       magenta
   EndFont
End

Style Question
  CopyStyle             Subsection*
  LatexName             question
End
