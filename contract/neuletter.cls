\NeedsTeXFormat{LaTeX2e}[1996/06/01]
\ProvidesClass{neuletter}%
	[2002/05/13 v0.7 LaTeX document class for NEU letters]
\RequirePackage{ifthen}
\newboolean{NU@lidofont} \setboolean{NU@lidofont}{false}
\DeclareOption{ec}{\setboolean{NU@lidofont}{false}}
\DeclareOption{ec}{} % it is default
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{letter}}
\ProcessOptions\relax

\LoadClass[12pt]{letter}

\ifthenelse{\boolean{NU@lidofont}}
  {\RequirePackage[T1]{fontenc}
	\RequirePackage{ofs}
	\RequirePackage{allfonts}
	% \@namedef is an internal LaTeX command which defines new
	% command with name extended by prepended backslash, i.e.,
	% \@namedef{cmd}{def} is equivalent of
	% \newcommand*{\cmd}{def}. However, no parameters are allowed.
	\newcommand{\newfamily}[2]{\@namedef{#1default}{\OFSfamily[#2]}}
	\newfamily{rm}{Lido}
	\newfamily{sf}{Helvetica}
	\newfamily{tt}{Courier}%
  }
  {\RequirePackage[T1]{fontenc}%
  }

\RequirePackage{url}

% it should be hugely intended
\setlength\oddsidemargin   {90.3pt}
  \setlength\evensidemargin  {\oddsidemargin}
\setlength\marginparsep {11pt}
\setlength\marginparpush{5pt}
\setlength\marginparwidth{36.14pt}
\addtolength\marginparwidth{-\marginparsep}

% and there should be big space for logo
\setlength\topmargin{28pt}
\setlength\headheight{12pt}
\setlength\headsep{20pt}
\setlength\footskip{25pt}

% Although this calculation is slower than use of direct numbers
% and \@p, it is much better maintainable.
\newlength{\headskip}
\setlength{\headskip}{2in}
\addtolength{\headskip}{-1in}
\addtolength{\headskip}{-\topmargin}
\addtolength{\headskip}{-\headheight}
\addtolength{\headskip}{-\headsep}
\addtolength{\headskip}{-\parskip}

% How much space should be left for signature (in \medskipamounts)
\newlength{\sigspace} \setlength{\sigspace}{12ex}

% Unfortunately, these lengths are already set in letter.cls
% as \newdimen, so I cannot use them as \newlength, although
% I would like to.
\longindentation=.45\textwidth
\indentedwidth=\textwidth
\advance\indentedwidth -\longindentation

\newlength{\sigindent}
	\setlength{\sigindent}{.4\longindentation}
\newlength{\sigwidth}
	\setlength{\sigwidth}{\indentedwidth}
	\addtolength{\sigwidth}{-\sigindent}

\renewcommand*{\opening}[1]{%
	\vspace*{\headskip}%
	\ifx\@empty\fromaddress
		\thispagestyle{firstpage}%
		{\raggedleft\@date\par}%
	\else  % home address
		\thispagestyle{empty}%
		{\raggedleft\begin{tabular}{l@{}}\ignorespaces
		\fromaddress \\*[2\parskip]%
		\@date \end{tabular}\par}%
	\fi
	\vspace{2\parskip}%
	{\raggedright \toname \\ \toaddress \par}%
	\vspace{2\parskip}%
	#1\par\nobreak%
}

\renewcommand{\closing}[1]{\par\nobreak\vspace{\parskip}%
  \stopbreaks
  \noindent
  \hspace*{\longindentation}%
  \parbox{\indentedwidth}{\raggedright%
       \ignorespaces #1\par \vspace{\sigspace}%
       \ifx\@empty\fromsig
           \relax
       \else
			 \hspace*{\sigindent}%
			 \parbox{\sigwidth}{\raggedright%
			 	\ignorespaces \fromsig}
		 \fi\strut}%
   \par%
}
