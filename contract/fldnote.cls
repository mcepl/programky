\NeedsTeXFormat{LaTeX2e}[1996/06/01]
\ProvidesClass{fldnote}%
	[2003/03/04 v0.1 LaTeX document class for field notes]
\RequirePackage{ifthen}
%\RequirePackage{trace}
\newboolean{FN@emptyspace} \setboolean{FN@emptyspace}{false}
\DeclareOption{prepare}{\setboolean{FN@emptyspace}{true}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}
\ProcessOptions\relax

% new preamble
\LoadClass{scrartcl}
\RequirePackage{titlesec}

\newlength{\FN@skip} \setlength{\FN@skip}{10cc}

% new commands and environments
\newcommand\FN@where{FN@undefined}
\newcommand\FN@who{FN@undefined}
\newcommand\FN@institution{FN@undefined}
\newcommand\FN@date{FN@undefined}
\newcommand\FN@phone{FN@undefined}
\newcommand\FN@position{FN@undefined}
\newcommand\FN@email{FN@undefined}
\newcommand\FN@address{FN@undefined}

\newcommand{\where}[1]{\renewcommand\FN@where{#1}}
\newcommand{\who}[1]{\renewcommand\FN@who{#1}}
\newcommand{\institution}[1]{\renewcommand\FN@institution{#1}}
\renewcommand{\date}[1]{\renewcommand\FN@date{#1}}
\newcommand{\phone}[1]{\renewcommand\FN@phone{#1}}
\newcommand{\position}[1]{\renewcommand\FN@position{#1}}
\newcommand{\email}[1]{\renewcommand\FN@email{#1}}
\newcommand{\address}[1]{\renewcommand\FN@address{#1}}

\renewcommand{\maketitle}{%
   \begin{center}
   		\LARGE{\bfseries\scshape Field Notes}
	\end{center}
   \bigskip\hfill \FN@date\par
   \bigskip
   \textbf{\large\sffamily\scshape \FN@who{}}, \texttt{\FN@email}\par
   \emph{\FN@institution{}},
   \ifthenelse{\equal{\FN@position}{FN@undefined}}
   	{}{\FN@position{},}
   \FN@phone{}\par
   \ifthenelse{\equal{\FN@address}{FN@undefined}}
   	{}{Address: \FN@address{}\par}
   \ifthenelse{\equal{\FN@where}{FN@undefined}}
   	{}{Interviewed at: \FN@where\par}
   \hrulefill\par
   \bigskip
}

\newcommand\question{
   \ifthenelse{\boolean{FN@emptyspace}}{%
	   \titlespacing{\subsubsection}{0pt}{0pt}{\FN@skip}
   }{}%
\subsubsection*}
