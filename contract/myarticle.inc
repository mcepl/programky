#% Do not delete the line below; configure depends on this
#  \DeclareLaTeXClass[article]{article (like from groff)}
# $Id$

# General textclass parameters
Input article.layout

ClassOptions
   Other "11pt"
End

Preamble
\def\do@url@hyp{\do\-}
\setlength{\overfullrule}{5pt}
\usepackage{verbatim}
\usepackage{rcs}
\usepackage[DIV10]{typearea}

\newcommand{\HR}{\bigskip%
        {\centering\noindent\large\sloppy * * *\par}%
        \bigskip%
}
\newlength{\mc@fnbodywidth}
\renewcommand{\@makefntext}[1]{%
    \setlength{\mc@fnbodywidth}{\linewidth}
    \addtolength{\mc@fnbodywidth}{-1.8em}
    \noindent\parbox[t]{1.8em}%
        {\hfill\@makefnmark\hspace*{3pt}}%
        \parbox[t]{\mc@fnbodywidth}{#1}%
}

\@ifclasswith{article}{cm}{%
   \usepackage{lmodern}%
   }{%
   % \usepackage{ofs}
   % \usepackage{allfonts}
   % \providecommand{\setfamily}[2]
   %    {\@namedef{#1default}{\OFSfamily[#2]}}
   % \providecommand{\setfontfamilies}[3]
   %    {\setfamily{rm}{#1}\setfamily{sf}{#2}\setfamily{tt}{#3}}
   % \setfontfamilies{Lido}{Helvetica}{Courier}

   \usepackage{mathptmx,helvet,courier}
   \renewcommand{\rmdefault}{sld}
}
   
\@ifclasswith{article}{draft}{%
   \usepackage{setspace}
   \doublespacing
   \usepackage[final]{geometry}   
}{}

\@ifundefined{newrefformat}{}{%
   \newrefformat{fot}{Footnote~\ref{#1}}
   %\let\oldprettyreg=\prettyref
   %\def{\renewcommand{\prettyref}[1]{\lowercase{\oldprettyref{#1}}}
}

\@ifpackagewith{natbib}{numeric}{%
   \@ifundefined{bibpunct}{}{%
      \bibpunct{[}{]}{;}{a}{,}{,}
   }
}

\newcommand{\fixme}[1]{\textsc{{[}FIXME: #1{]}}}
\usepackage{booktabs}


EndPreamble

# Title style definition
Style Title
  Margin                Static
  LatexType             Command
  InTitle               1
  LatexName             title
  ParSkip               0.4
  ItemSep               0
  TopSep                0
  BottomSep             1
  ParSep                1
  Align                 Center
  AlignPossible         Center
  LabelType             No_Label
  Preamble
   \let\@subtitle\@empty
   \def\@maketitle{%
     \newpage
     \null
     \vskip 2em%
     \begin{center}%
     \let \footnote \thanks
       {\LARGE \bfseries \@title \par}%
         \ifx\@subtitle\@empty
         \else
            \vskip 1em%
            {\Large \@subtitle}
         \fi
       \vskip 1.5em%
       {\large
         \lineskip .5em%
         \begin{tabular}[t]{c}%
           \@author
         \end{tabular}\par}%
       \vskip 1em%
       {\large \@date}%
     \end{center}%
     \par
     \vskip 1.5em}
  EndPreamble

  # standard font definition
  Font
    Size                Largest
  EndFont

End

Style Subtitle
  Margin                Static
  LatexType             Command
  InTitle               1
  LatexName             subtitle
  ParSkip               0
  ItemSep               0
  TopSep                -1
  BottomSep             1
  ParSep                1
  Align                 Center
  AlignPossible         Center
  LabelType             No_Label
  Preamble
   \newcommand{\subtitle}[1]{\def\@subtitle{#1}}
  EndPreamble

  # standard font definition
  Font
    Size                Larger
  EndFont

End

Style Comment
	CopyStyle			 Comment
	Preamble
	\usepackage{color}
	\definecolor{lightgray}{gray}{0.9}
   \newif\ifcommentvisible

   \@ifclasswith{article}{draft}{%
      \commentvisibletrue%
   }{%
      \commentvisiblefalse%
   }
   \ifcommentvisible
      \renewenvironment{comment}%
      {\color{red}\begin{sffamily} \begin{scshape}}
         {\end{scshape} \end{sffamily}\color{black}\par}
   \else
   \fi
EndPreamble
End

Style Variables
  Margin		First_Dynamic
  LatexType		List_environment
  LatexName		variables
  NextNoIndent		1
  LeftMargin		MM
  LabelSep		xxx
  ParSkip		0
  ItemSep		0
  TopSep		0
  BottomSep		0
  ParSep		0
  Align			Block
  AlignPossible		Block, Left
  LabelType		Manual
  LabelString		MMM
  Preamble
   \newenvironment{variables}[1]
      {\begin{list}{%
         \renewcommand{\makelabel}[1]{\hspace\labelsep \normalfont #1}
         }{%
            \setlength{\topsep}{0pt}
            \setlength{\itemsep}{0pt}
            \setlength{\parsep}{0pt}
            \setlength{\labelsep}{2ex}
            \settowidth{\labelwidth}{#1}
            \setlength{\leftmargin}{\parindent}
            \addtolength{\leftmargin}{\labelwidth}
            \addtolength{\leftmargin}{\labelsep}
         }%
      }
      {\end{list}}
  EndPreamble
End

# vim:set ft=tex:
