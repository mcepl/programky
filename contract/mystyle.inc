#% Do not delete the line below; configure depends on this
#  \DeclareLaTeXClass[article]{article (like from groff)}
# $Id$

# General textclass parameters
Input article.layout

ClassOptions
   Other "11pt"
End

Preamble
\setlength{\overfullrule}{5pt}
\usepackage{typearea}
\usepackage{rcs}
\usepackage{ifpdf}

\newcommand{\HR}{\bigskip%
        {\centering\noindent\large\sloppy * * *\par}%
        \bigskip%
}

\newlength{\mc@fnbodywidth}
\renewcommand{\@makefntext}[1]{%
    \setlength{\mc@fnbodywidth}{\textwidth}
    \addtolength{\mc@fnbodywidth}{-1.8em}
    \noindent\parbox[t]{1.8em}%
        {\hfill\@makefnmark\hspace*{3pt}}%
        \parbox[t]{\mc@fnbodywidth}{#1}%
}

\def\@maketitle{%
  \newpage
  \null
  \vskip 2em%
  \begin{center}%
  \let\footnote=\thanks
    {\huge\bfseries \@title \par}%
    \vskip 1.5em%
    {\large
      \lineskip .5em%
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}\par}%
    \vskip 1em%
    {\large \@date}%
  \end{center}%
  \par
  \vskip 1.5em}
EndPreamble
