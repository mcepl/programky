#!/usr/bin/python
"""
Copyright (c) 2000 Matej Cepl <matej@ceplovi.cz>
   
Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

-------

Process email from the euroclub e-mail list.

$Id$
$Log$
Revision 1.5  2002/11/17 21:09:37  matej
Adresa zm��žn��žna na ceplovi.cz

Revision 1.4  2001/11/25 20:33:03  matej
AdresĂ��ž�š euroclub se d��žlĂ��ž p�šĂ­mo v domĂ��žcĂ­m adresĂ��ž�ši.

Revision 1.3  2001/09/20 21:43:14  matej
Licence and some cleaning up.

Revision 1.2  2001/08/21 20:58:09  user
Opraveny drobne preklepy.

Revision 1.1  2001/08/21 16:11:38  user
Initial revision

"""

import sys, string
import os, os.path

# adresar, do ktereho se to vsechno rozbali
dir_save_to = os.path.expanduser("~/euroclub/")

def rep_string(str,num):
    """Return string from num repeating substring str.

    There should be something like it in library, but I do not
    know where.
    """
    outstr = ""
    for i in range(num):
        outstr = outstr + str
    return outstr

def split_body(text):
    """Split the whole message into individual e-mails.

    Gets a body of e-mail as a parametr
    and returns list of all emails in that.
    """
    cut_string= '\n' + rep_string('=',62) + '\n'
    # we needn't header of the original message
    return string.split(text,cut_string)[1:]
    # hopefully, resulting list will be free of cut_string
    # otherwise, you should clear it NOW

def get_filename(sub_line):
    sys.stderr.write(sub_line + ', ')
    tmp = sub_line[len("Subject:"):] #
    cut = string.find(tmp,"From: ")
    if not(cut):
        tmp = tmp[:cut]
    tmp = string.replace(tmp,' ','_')
    tmp = string.replace(tmp,'/','-')
    tmp = string.replace(tmp,'*','')
    tmp = string.replace(tmp,'!','')
    tmp = string.replace(tmp,'=','')
    tmp = string.replace(tmp,'#','')
    tmp = string.replace(tmp,'@','')
    tmp = string.strip(tmp)
    sys.stderr.write(tmp + '\n')
    return tmp

def clean_nonsense(text):
    """Clean a message from the advertising strings.
    """

    msn_nonsense = rep_string('_',65) + '\n' + """
    Get your FREE download of MSN Explorer at
    http://explorer.msn.com/intl.asp
    """

    y_nonsense = rep_string('_',50) + '\n' + "Do You Yahoo!?\n"

    tmp = string.replace(text,msn_nonsense,'')
    # A ted Yahoo! -- to ale zpravy vymenuje
    y_how_many = string.count(text,y_nonsense)
    for i in range(y_how_many):
        y_where_beg = string.find(text,y_nonsense)
        y_where_end = string.find(text,'\n\n',y_where_beg)
        y_where_from = string.find(text,'\nFrom',y_where_beg,y_where_end)
        if not(y_where_from): # Beware of too tightly
                              # conactecated messages
            y_where_end = y_where_from
        text = text[:y_where_beg] + text[y_where_end:]
    return text

if __name__ == "__main__":
    if not (os.path.exists(dir_save_to)):
        os.mkdir(dir_save_to)
    email = sys.stdin.read()
    email = clean_nonsense(email)
    messages = split_body(email)
    for msg in messages:
        sub_from = string.find(msg,"Subject: ") +1 # nechci konec radku
        sub_to = string.find(msg,"\n",sub_from) # nechci konec radku
        try:
            fname = msg[sub_from:sub_to]
        except IndexError:
            raise IndexError, 'ERROR: No Subject line!!!'
        fname = os.path.join(dir_save_to,get_filename(fname))
        file = open(fname,'w')
        file.write(msg+'\n')
        file.close()
