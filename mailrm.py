#!/usr/bin/env python
#$Id$
debug = 0
import os,sys

msgdellist = []
num = 0
inpipe = os.popen("/usr/sbin/exim4 -bp","r")
for lineraw in inpipe:
    lineraw = lineraw.rstrip()
    if debug: print >>sys.stderr,lineraw
    line = lineraw.strip().split()
    if len(line) >= 3:
        num += 1
        msgdellist.append(line[2])
        print "%d: %s\t%s" % (num,line[2],line[3])
    elif len(line) >= 0:
        print lineraw
inpipe.close()

if debug: print >>sys.stderr,msgdellist
if len(msgdellist) > 0:
    sys.stdout.write("Which message should be deleted: ")
    chr = sys.stdin.readline().strip()
    if len(chr) == 0:
        raise ValueError, "No answer."
    chr = int(chr)-1 # list is indexed from 0, but presented from 1
    if chr == -1:
        sys.exit()
    if (chr < 0) or (chr > len(msgdellist)):
        raise ValueError, "Inproper answer."
    os.system("/usr/sbin/exim4 -Mrm %s" % msgdellist[chr])