var line=new String();
var newLine=new String();

var lineNo=view.cursorLine();
var columnNo=view.cursorColumn();
line=document.textLine(lineNo);
document.removeLine(lineNo);
if (line.search(/^\s*\[.\]/) == -1) {
	head=line.replace(/^(\s*).*$/,'$1');
	tail=line.replace(/^\s*(.*)$/,'$1');
    newLine=head+'[_] '+tail;
} else { newLine = line ; }
document.insertLine(lineNo,newLine);
view.setCursorPosition(lineNo,columnNo);