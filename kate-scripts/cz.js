// small helpful utilities
function dbg(str) {
    document.insertText(view.cursorLine(),view.cursorColumn(),str);
}

function setLine(number,text) {
    res1 = document.removeLine(number);
    res2 = document.insertLine(number,text);
}

function getIndentLevel(line) {
    completeString = document.textLine(line);
    bareString = completeString.replace(/^\t*(.*)$/,"$1");
//     println(line+": "+String(completeString.length-bareString.length));
    return (completeString.length-bareString.length);
}

function findRootParent(line) {
    var currentIndent = getIndentLevel(line);
    if (currentIndent == 0) {
        return (line);
    }
    i = line
    while ((i > 1) && (getIndentLevel(i) > 0s)) {
        i--;
    }
    return (i);
}

// InsertCheckBox()
// Insert a checkbox at the beginning of a header without disturbing the
// current folding.
// This is a brutal hack -- optional arguments to the function in JavaScript!
// Don't declare any arguments in the function declaration:
function insertCheckBox() {
	var CBox = "[_] ";
    // Instead, use the intrinsic arguments object to retrieve
    // arguments.
    var lineNo = arguments[0];
	if (arguments.length > 1) {
		CBox = arguments[1];
	}
	var originalLine = document.textLine(line);
	var newLine = originalLine.replace(/^(\t*)(.*)$/,"$1"+CBox+"$2");
	setLine(line,newLine);
}

// safely InsertCheckBox()
// Insert a checkbox at the beginning of a header without disturbing the
// current folding only if there is no checkbox already.
function safelyInsertCheckBox(line) {
	if (! document.textLine(line).test(/^\t+[<>:;|]/)) {
		if (! document.textLine(line).test(/^\t*\[[X-_]\]/)) {
			insertCheckBox(line);
		}
	}
}

// safely InsertCheckBoxPercent() {{{1
// Insert a checkbox and % sign at the beginning of a header without disturbing 
// the current folding only if there is no checkbox already.
function safelyInsertCheckBoxPercent(line) {
	if (! document.textLine(line).test(/^\t+[<>:;|]/)) {
        if (! document.textLine(line).test(/^\t*\[[X-_]\]/)) {
			if (getIndentLevel(line+1) > getIndentLevel(line)) {
				incX = "[_] % ";
			} else {
				incX = "[_] ";
			}
			insertCheckBox(line,incX);
        }
	}
}

// Safely InsertCheckBoxPercentAlways()
// Insert a checkbox and % sign at the beginning of a header without disturbing 
// the current folding only if there is no checkbox already. Include the 
// checkbox even on childless headings.
function safelyInsertCheckBoxPercentAlways() {
	if (! document.textLine(line).test(/^\t+[<>:;|]/)) {
        if (! document.textLine(line).test(/\[[X_]\]/)) {
			insertCheckBox(line,"[_] % ");
		}
	}
}

// SwitchBox()
// Switch the state of the checkbox on the current line.
function switchBox(line) {
	if (document.textLine(line).test(/\[_\]/) || document.textLine(line).test(/\[X\]/)) {
		if (document.textLine(line).test(/\[_\]/)) {
			newLine = document.textLine.replace(/\[_\]/,"[X]");
		}  else {
			newLine = document.textLine.replace(/\[X\]/,"[_]");
		}
	}
}

// DeleteCheckbox()
// Delete a checkbox if one exists
function deleteCheckbox(line) {
	var originalLine = document.textLine(line);
	var newLine = originalLine;
	if (originalLine.test(/\[_\]/) || originalLine.test(/\[X\]/)) {
		newLine = originalLine.replace(/^(\t*)\[[X_]\] (.*)$/,"$1$2");
	}
	setLine(line,newLine);
}

// line is a number of the current line
function NewHMD(line) {
    var done = 0;
    var countDone = 0;
    var countChildren = 0;
    var i = 1;
    var indLine = getIndentLevel(line);
    while (indLine < getIndentLevel(line+i)) {
        countChildren = countChildren + 1;
        if ((getIndentLevel(line)+1) == getIndentLevel(line+i)) {
            childdoneness = NewHMD(line+i);
            if (childdoneness >= 0) {
                done = done + childdoneness;
//                 dbg(done);
                countDone = countDone + 1;
//                 dbg(count);
            }
        }
        i = i + 1;
    }
    var proportion=0;
    if (countDone > 0) {
        proportion = ((done * 100)/countDone)/100;
    } else {
        if (document.textLine(line).search(/\[X\]/) != -1) {
            proportion = 100;
        } else {
            proportion = 0;
        }
    }
    setLine(line,document.textLine(line).replace(" [0-9]*%"," "+proportion+"%"));
    if (proportion == 100) {
        setLine(line,document.textLine(line).replace("\[.\]","[X]"));
        return 100;
    }
    else if (proportion == 0 && countDone == 0) {
        if (document.textLine(line).search("\[X\]") != -1) { return 100; }
        else if (document.textLine(line).search("\[_\]") != -1) { return 0; }
        else { return -1; }
    }
    else {
        setLine(line,document.textLine(line).replace("\[.\]","[_]"));
        return proportion;
    }
}

var lineNo=view.cursorLine();
document.editBegin();
var curLineHMD = NewHMD(lineNo);
document.editEnd();