var line=new String();
var newLine=new String();

var lineNo=view.cursorLine();
var columnNo=view.cursorColumn();
line=document.textLine(lineNo);
document.removeLine(lineNo);
if (line.search(/\[_\]/) != -1) {
    newLine=line.replace(/\[_\]/,'[X]');
} else { if (line.search(/\[X\]/) != -1) {
    newLine=line.replace(/\[X\]/,'[_]');
    } else { newLine = line ; }
}
document.insertLine(lineNo,newLine);
view.setCursorPosition(lineNo,columnNo);