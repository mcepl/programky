#!/usr/bin/env python
import email
import email.Errors
import email.Header
import mailbox
import codecs
import sys

class MyMbox(mailbox.Maildir):
   def __init__(self,dirname):
      mailbox.Maildir.__init__(self,dirname,email.message_from_file)
      self.dirname = dirname
      self.decfunc = email.Header.decode_header
      self.msg = ""

   def __msgfactory(self,fp):
       try:
           return email.message_from_file(fp)
       except email.Errors.MessageParseError:
       # Don't return None since that will
       # stop the mailbox iterator
           return ''

   def __translateHeader(self,headerName):
      header = email.Header.decode_header(self.msg[headerName])
      string = header[0][0]
      encoding = header[0][1]
      if not(encoding):
         encoding = "ascii"
      outstr = string.decode(encoding,'ignore')
      return outstr

   def listHeaders(self):
      for self.msg in self:
          #hdrfrom = self.__translateHeader("From")
          #hdrto = self.__translateHeader("To")
          #hdrdate = self.__translateHeader("Date")
          #hdrsubject = self.__translateHeader("Subject")
          #print "%s;%s;%s" % (hdrfrom,hdrdate,hdrsubject)
          header = email.Header.decode_header(self.msg["Message-Id"])[0][0]
          print "%s;%s" % (self.dirname,header)

if __name__=="__main__":
    obj = MyMbox(sys.argv[1])
    obj.listHeaders()
