#!/usr/bin/python
# $Id$

sub &usage {
die <<EOF;
Usage: w2i [-i|-l|-r|-w] filename
Translate file filename from one to other Czech coding.
-i from M\$ CP1250 to ISO 8859-2 (default)
-a from ISO 8859-2 to plain ASCII
-l from PC Latin 2 to ISO 8859-2
-r, -w from ISO 8859-2 to M\$ CP1250
EOF
}

# system dependent -- create some individual tempfile
$tempfile = `mktemp /tmp/w2i.XXXXXX`;

%smer_prekl=('-l','pc2 il2 ','-r','il2 1250 ','-w','il2 1250 ',\
'-i','1250 il2 ','-a','il2 ascii ');

if ($#_ == 2) {
    $soubor = $_[1];
    $parametry = $smer_prekl{$_[0]};
}
elsif ($#_ == 1) {
    $soubor = $_[0];
    $parametry = $smer_prekl["-i"];
}
else { &usage; }

$result = system "cstocs $parametry $soubor > $tempfile 2> /dev/null";

# Depends on results of CSTOCS
if (result == 0) {
    # do proceed only when CSTOCS suceeded
    unlink $soubor;
    copy $tempfile, $soubor;
    unlink $tempfile;
else {
    unlink $tempfile;
    die "Something wrong happened -- exiting!\n";
}




