#!/usr/bin/perl -w
#$Id$

$0 =~ s/.*\///; $prog = $0;
$prog =~ s/vln//;

$file = $ARGV[0];
#open(OLD,"<$file") || die "Can't open input file $file $!";
$tempfile = `mktemp /tmp/vlnlatex.XXXXXX`;
chomp $tempfile;

$newname = $file;
$newname =~ s/\.tex$/\.cstex/;

#open(NEW,">$tempfile") || die "Can't open output file $newname $!";
#while(<OLD>) {
#    s/(^|[^?!])\{\}``/$1``/g; # erase superfluous empty groups
#	 s/\b([KkSsvVZzOoUuÂ§])([ ]|\$)/$1~/g; # substitute for vlna
#    print NEW;
#}
#close(OLD);
#close(NEW);
#system "mv $tempfile $newname";
system "vlna -s -v KkSsVvZzOoUuAÂ§ -f $file $newname";

system "$prog $newname";
unlink $tempfile || die "Can't delete temporary file $tempfile $!";
