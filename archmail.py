#!/usr/bin/env python
# -*- coding: iso-8859-2 -*-
"""
Script for archiving mail messages which are older than
configured time to another copy of mailbox structure (original is
in Maildir folders, and archived into mbox files).

$Id$
"""

import email,email.Errors,email.Utils,mailbox
import os,os.path
import sys,re,time
import archive_folder

__author__ = 'Matej Cepl <matej@ceplovi.cz>'
__date__ = '$Date$'
__version__ = '$Revision$'

mdir = os.path.expanduser("~/.mail/")

def archivebox(procbox):
   """
   Archive Maildir mailfolder identified by its directory name to
   archive directory. Using native Python method of archiving
   mailbox. If the archive folder or its directory does not
   exist, it creates them.
   """
   wbox=re.sub('/cur','',procbox)
   archbox=re.sub(mdir+r'\.(.+)',archdir+r'/\1',wbox)
   archbox=re.sub(r'/(.*)\.directory',r'/\1',archbox).lower()
   basendir=os.path.dirname(archbox)
   if not(os.path.exists(basendir)):
      os.makedirs(basendir)
   if not(os.path.exists(archbox)):
      os.system('touch '+archbox)
   print >>sys.stderr,wbox
   box = archive_folder.Mybox(wbox)
   box.archive(atime,archbox)
   box.flush()

def walking(arg,dirname,names):
    """
    Main processing of mail store (archiving mailboxes, skipping
    mailfolders, which cannot be archived, and moving outgoing
    messages).
    """
    if os.path.isdir(dirname) \
        and (dirname[-3:] == "cur") \
        and len(os.listdir(dirname)) != 0 \
        and (dirname != mdir + "Computers/cur") \
        and (dirname != mdir + "drafts/cur") \
        and (dirname != mdir + "fast_trash/cur") \
        and (dirname != mdir + "inbox/cur") \
        and (dirname != mdir + "_junk/cur") \
        and (dirname != mdir + "NEU/cur") \
        and (dirname != mdir + "outbox/cur") \
        and (dirname != mdir + "Pratele/cur") \
        and (dirname != mdir + "sent-mail/cur") \
        and (dirname != mdir + "trash/cur") \
        and (dirname != mdir + "_unsure/cur") \
        and (dirname != mdir + "Work/cur"):
            archivebox(dirname)

if __name__ == "__main__":
   # Calculate archive and trash date into Python time tuple
   todaytime = list(time.localtime())
   todaytime[1] -= 3
   atime = tuple(todaytime)

   # Calculate directory for archiving
   archdir= os.path.expanduser("~/archiv/"+time.strftime("%Y",atime)+"/mail")

   # do the work itself
   os.path.walk(mdir,walking,"")