#!/usr/bin/env python
"""Usage: ogg2mp3 filename
Converts filename from OGG format into MP3 format preserving
all metainformation."""
# $Id$
#                                  MIT License
#
#     Copyright (c) 2005 Matej Cepl <matej@ceplovi.cz>
#
#     Permission is hereby granted, free of charge, to any person obtaining
#     a copy of this software and associated documentation files (the
#     "Software"), to deal in the Software without restriction, including
#     without limitation the rights to use, copy, modify, merge, publish,
#     distribute, sublicense, and/or sell copies of the Software, and to
#     permit persons to whom the Software is furnished to do so, subject to
#     the following conditions:
#
#     The above copyright notice and this permission notice shall be
#     included in all copies or substantial portions of the Software.
#
#     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#     IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#     CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#     TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#     SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import ogg.vorbis, os, sys, locale
locale.setlocale(locale.LC_ALL,'')

known_genre = ["A Cappella","Acid","Acid Jazz", "Acid Punk", "Acoustic",
    "Alternative", "Alt. Rock", "Ambient", "Anime", "Avantgarde", "Ballad",
    "Bass", "Beat", "Bebob", "Big Band", "Black Metal", "Bluegrass", "Blues",
    "Booty Bass", "BritPop", "Cabaret","Celtic","Chamber Music","Chanson",
    "Chorus", "Christian Gangsta Rap", "Christian Rap", "Christian Rock",
    "Classical", "Classic Rock", "Club", "Club-House","Comedy",
    "Contemporary Christian","Country","Crossover","Cult","Dance","Dance Hall",
    "Darkwave", "Death Metal", "Disco", "Dream", "Drum & Bass", "Drum Solo",
    "Duet", "Easy Listening","Electronic","Ethnic","Eurodance","Euro-House",
    "Euro-Techno","Fast-Fusion","Folk","Folklore","Folk/Rock","Freestyle",
    "Funk","Fusion","Game","Gangsta Rap","Goa","Gospel","Gothic","Gothic Rock",
    "Grunge","Hardcore","Hard Rock","Heavy Metal","Hip-Hop","House","Humour",
    "Indie","Industrial","Instrumental","Instrumental Pop","Instrumental Rock",
    "Jazz","Jazz+Funk","JPop","Jungle","Latin","Lo-Fi","Meditative","Merengue",
    "Metal","Musical","National Folk","Native American","Negerpunk","New Age",
    "New Wave","Noise","Oldies","Opera","Other","Polka","Polsk Punk","Pop",
    "Pop-Folk","Pop/Funk","Porn Groove","Power Ballad","Pranks","Primus",
    "Progressive Rock","Psychedelic","Psychedelic Rock","Punk","Punk Rock",
    "Rap","Rave","R&B","Reggae","Retro","Revival","Rhythmic Soul","Rock",
    "Rock & Roll","Salsa","Samba","Satire","Showtunes","Ska","Slow Jam",
    "Slow Rock","Sonata","Soul","Sound Clip","Soundtrack","Southern Rock",
    "Space","Speech","Swing","Symphonic Rock","Symphony","Synthpop","Tango",
    "Techno","Techno-Industrial","Terror","Thrash Metal","Top 40","Trailer",
    "Trance","Tribal","Trip-Hop""Vocal"]

def mp(dict,key,para):
    if dict.has_key(key):
        value = unicode(dict[key][0])
        value = value.encode(locale.getpreferredencoding())
        if (value == ""):
            return ""
        else:
            return '--%s "%s"' % (para,value)
    else:
        return ""

def usage():
    print __doc__
    sys.exit(-1)

if ((len(sys.argv) != 2) or (sys.argv[1] == "-h") or (len(sys.argv[1]) == 0)):
    usage()
else:
    infile=sys.argv[1]
oggfile = ogg.vorbis.VorbisFile(infile)
outfile=infile.split("/")[-1].split(".")[:-1][0]+".mp3"
ogginfo = oggfile.comment().as_dict()
if not(ogginfo["GENRE"] in known_genre) and ("GENRE" in ogginfo):
    del ogginfo["GENRE"]
decpart = 'ogg123 -q -d wav -f - -v "%s"' % infile
paras = "--quiet %s %s %s %s %s %s %s" % (mp(ogginfo,'TITLE','tt'),
    mp(ogginfo,'ARTIST','ta'),
    mp(ogginfo,'ALBUM','tl'),
    mp(ogginfo,'DATE','ty'),
    mp(ogginfo,'DESCRIPTION','tc'),
    mp(ogginfo,'TRACKNUMBER','tn'),
    mp(ogginfo,'GENRE','tg'))
encpart = 'lame %s - "%s"' % (paras,outfile)
system = os.system("%s | %s" % (decpart,encpart))
